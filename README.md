# The Internet

This project implements the software for specific devices and Protocols that are present in the internet today.

1. TCP Stop and Wait Protocol.
2. TCP Sliding Window Protocol.
3. A simple router
4. A simple router with a network address translator (NAT)

## Disclaimer: 
This projects is based on the codebase found here in these locations: 
- [https://bitbucket.org/cs144-1617/lab12/src/b70833e817272ceef5cab25c777e27aba1be3b0c?at=master](https://bitbucket.org/cs144-1617/lab12/src/b70833e817272ceef5cab25c777e27aba1be3b0c?at=master)
- [https://bitbucket.org/cs144-1617/lab3/src/abc24f8f537dcd3875629a3c91344637b7581abd/router/sr_router.c?at=master&fileviewer=file-view-default](https://bitbucket.org/cs144-1617/lab3/src/abc24f8f537dcd3875629a3c91344637b7581abd/router/sr_router.c?at=master&fileviewer=file-view-default)

Many thanks to Professor Phil, professor Nick and all the [cs144](https://lagunita.stanford.edu/courses/Engineering/Networking-SP/SelfPaced/info) staff. 

## Requirments: 

To build and run this code successully, you will need to install a virtual machine that runs with ubuntu and has mininet installed and the network card configured.Such a virtual machine can be found on [here](http://web.stanford.edu/class/cs144/assignments/vm/vm-setup.html) with the installation manual.

## Compilation:

After installing the VM and cloning the repository in the virtual machine, you will need to compile the code. To do so follow this procedure: 

1. open the terminal. 
2. cd /path/to/repo/ctcp/SlidingWindow
3. Type 'make' into the terminal and press enter. 
4. cd /path/to/repo/ctcp/StopAndWait. 
5. repeat step 3.
6. cd /path/to/repo/router/simpleRouter/router
7. make
8. cd /path/to/repo/router/NAT-router/router
9. make

Now you have compiled all the components.

## Usage (TCP):

To run the ctcp as a server you will you will navigate in the terminal using 'cd' to either  /path/to/repo/ctcp/SlidingWindow or  /path/to/repo/ctcp/StopAndWait and run the following commands

`sudo ./ctcp -s -p PORT` 

Where PORT can be any number between 1 and 2^16

To run the ctcp as a client run the following command

`sudo ./ctcp -c www.website.com:PORT -p PORT`

This will establish a connection with the server. The program will listen to inputs on STDIN and will send it once you type Enter. The program sends FIN packet and terminates when reading EOF.

For more info take a look at this [document](https://bitbucket.org/cs144-1617/lab12/raw/b70833e817272ceef5cab25c777e27aba1be3b0c/README).


---------------------------------

To run the router you will you will navigate in the terminal using 'cd' to /path/to/repo/router/NAT-router and run the following command to start the mininet emulation system. 

`./run_all.sh`

After running this, mininet will create the following topology, where you are the client with IP address 10.0.1.100 and there are two servers as show. 

![Affinity Diagram](http://web.stanford.edu/class/cs144/assignments/nat/diagram.png "Diagram")

To start the router without the NAT being enabled navigate to /path/to/repo/router/NAT-router/router on the terminal and run the following command

`./sr`

Otherwise to enable the NAT you will need to add a flag

`./sr -n`

Note that you can control the NAT mapping timeout with the following options

-I INTEGER -- ICMP query timeout interval in seconds (default to 60)
-E INTEGER -- TCP Established Idle Timeout in seconds (default to 7440)
-R INTEGER -- TCP Transitory Idle Timeout in seconds (default to 300)


Now the router is running. To test it you can open another terminal and try to  ping any of the servers using their IP addresses or even stablish a connection with them using tools like curl and wget.


## What's different than the codebase: 

#### TCP:
I have added all the tcp logic in the ctcp.c file and ctcp.h

#### router
I have added all the router logic in the router.c and router.h files. Moreover, I have created additional files and structure for the NAT. Namely, sr_nat.c sr_nat.h sr_nat_router.c sr_nat_router.h

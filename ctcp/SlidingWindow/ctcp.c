/******************************************************************************
 * ctcp.c
 * ------
 * Implementation of cTCP done here. This is the only file you need to change.
 * Look at the following files for references and useful functions:
 *   - ctcp.h: Headers for this file.
 *   - ctcp_iinked_list.h: Linked list functions for managing a linked list.
 *   - ctcp_sys.h: Connection-related structs and functions, cTCP segment
 *                 definition.
 *   - ctcp_utils.h: Checksum computation, getting the current time.
 *
 *****************************************************************************/

#include "ctcp.h"
#include "ctcp_linked_list.h"
#include "ctcp_sys.h"
#include "ctcp_utils.h"

/**
 * Connection state.
 *
 * Stores per-connection information such as the current sequence number,
 * unacknowledged packets, etc.
 *
 * You should add to this to store other fields you might need.
 */
struct ctcp_state {
  struct ctcp_state *next;  /* Next in linked list */
  struct ctcp_state **prev; /* Prev in linked list */

  conn_t *conn;             /* Connection object -- needed in order to figure
                               out destination when sending */
  linked_list_t *segments;  /* Linked list of segments sent to this connection.
                               It may be useful to have multiple linked lists
                               for unacknowledged segments, segments that
                               haven't been sent, etc. */

	linked_list_t* recv_segments; /*Linked list that contains all the received and unacknowledged segments*/
	ll_node_t* out_seg; /*points to the last segment passed to STDOUT from the received segments*/
	linked_list_t* queued_segments; /*segments that couldn't be send becuase of the window size*/
 	ctcp_config_t* cfg; /*configuration of the connection*/
	int n_transmissions; // number of transmissions
		
	int time; /*the time since the last transmision of the segment*/
	
	bool peer_conn; /*The state of the other side of the connection*/
	bool my_conn; /*the state of my connection*/
	uint32_t seqno;    // sequence number of the current connection
	uint32_t ackno;    // acknowledgement number of the current connection;
	int curr_send_window; // how much bytes can be currently send to the other host

	
};

/**
  * LinkedList -> Void
  * Frees all the segments in a linked list
  */
void destroy_listObjects(linked_list_t* ll)
{
	ll_node_t* curr = ll_front(ll);
	while(curr != NULL)
	{
		free(curr->object);
		curr = curr->next;
	}
}

/**
 * Linked list of connection states. Go through this in ctcp_timer() to
 * resubmit segments and tear down connections.
 */
static ctcp_state_t *state_list;

/* FIXME: Feel free to add as many helper functions as needed. Don't repeat
          code! Helper functions make the code clearer and cleaner. */


/**
  * State -> Int
  * Sends segments from the queue segment depending on how much window space is available 
  * returns the number of segments sent
  */
int send_queued(ctcp_state_t* state)
{
	int i = 0;
	ll_node_t* curr = ll_front(state->queued_segments);
	// keeps running as long as the otherside can receive data
	while(state->curr_send_window != 0 && curr != NULL)
	{
		ctcp_segment_t* tmp = curr->object;
		if(state->curr_send_window >= ntohs(tmp->len) - sizeof(ctcp_segment_t))
		{
			// sends data and removes it from the queue list and adds it to the segments list
			conn_send(state->conn, tmp, ntohs(tmp->len));
			state->curr_send_window -=  ntohs(tmp->len) - sizeof(ctcp_segment_t);
			ll_remove(state->queued_segments, curr);
			ll_add(state->segments, tmp);
			i++;
		}
			curr = curr->next;
	}
	return i;
}

/**
  * State -> Int
  * Sends segments that has not been acknowledged
  * returns the number of segments sent
  */
int send_segments(ctcp_state_t* state)
{
	int i = 0;
	ll_node_t* curr = ll_front(state->segments);
	// keeps running as long as the otherside can receive data
	while(curr != NULL)
	{
		ctcp_segment_t* tmp = curr->object;
		
			// sends data and removes it from the queue list and adds it to the segments list
			conn_send(state->conn, tmp, ntohs(tmp->len));
			i++;
			curr = curr->next;

	}
	return i;
}


/**
  * ListOfSegment* , segment* -> node*
  * returns the node which contains the ctcp segment that belongs to the received acknowledgment
  * returns NULL if not found
  */
ll_node_t* ack_segment(linked_list_t* segments, ctcp_segment_t* acknowledgment)
{
	ll_node_t* curr = ll_front(segments);
	while(curr != NULL)
	{
		ctcp_segment_t* tmp = curr->object;
		if(ntohl(tmp->seqno) + ntohs(tmp->len) - sizeof(ctcp_segment_t) == ntohl(acknowledgment->ackno) || 
		((ntohl(tmp->flags) & FIN) && (ntohl(tmp->seqno) + ntohs(tmp->len) - sizeof(ctcp_segment_t) +1 == ntohl(acknowledgment->ackno) )))
		{
			return curr;
		}
		else
		{
			curr = curr->next;
		}
	}
	return NULL;
}

/**
  * ListOfSegment* , segment* -> node*
  * returns the node which the given segment should be placed after
  * returns NULL if this segment doesnot belong to this connection
  */
ll_node_t* ctcp_prev_segment(linked_list_t* segments, ctcp_segment_t* segment)
{
	ll_node_t* curr = ll_front(segments);
	while(curr != NULL)
	{
		ctcp_segment_t* tmp = curr->object;
		if(ntohl(tmp->seqno) + ntohs(tmp->len) - sizeof(ctcp_segment_t) == ntohl(segment->seqno))
		{	
			if(curr->next != NULL && ((ctcp_segment_t*)curr->next->object)->seqno == tmp->seqno)
			{
				return NULL;
			}
			else
			{
				return curr;
			}	
		}
		else
		{
			curr = curr->next;
		}
	}
	return NULL;
}
/**
  * State*, char*, Int -> Segment*
  * Creates a new segment with the given buffer as the payload and returns a pointer to this segment
  */
ctcp_segment_t* ctcp_create_segment(ctcp_state_t* state,char* buffer, int n_bytes)
{
	/* creates a new tcp segment */
	ctcp_segment_t* segment = malloc(sizeof(ctcp_segment_t) + n_bytes);
	segment->seqno = htonl(state->seqno);
	segment->ackno = htonl(state->ackno);
	segment->len = htons(n_bytes + 20);
	segment->flags = TH_ACK; 
	segment->window = htons(state->cfg->recv_window);
 	segment->cksum = 0;
	memcpy(segment->data, buffer, n_bytes);
	segment->cksum = cksum(segment, n_bytes + sizeof(ctcp_segment_t));	// calculates the check sum 
	return segment;
}
/**
  * State, flags -> void
  * given a state object, sends a FIN packet to the connection associated with the state
  *
  */
void sendFin(ctcp_state_t *state)
{
	ctcp_segment_t* segment = malloc(sizeof(ctcp_segment_t));
	segment->seqno = htonl(state->seqno);
	segment->ackno = htonl(state->ackno);
	segment->len = htons(sizeof(ctcp_segment_t));
	segment->flags = 0;
	segment->flags |= htonl(FIN);
	segment->window = htons(state->cfg->recv_window);
 	segment->cksum = 0;
	segment->cksum = cksum(segment, sizeof(ctcp_segment_t));	// calculates the check sum 
	conn_send(state->conn, segment, sizeof(ctcp_segment_t));
	ll_add(state->segments, segment);
}
/**
  * State -> void
  * given a state object, sends a ACK packet to the connection associated with the state
  *
  */
void sendAck(ctcp_state_t *state)
{
	ctcp_segment_t* segment = malloc(sizeof(ctcp_segment_t));
	segment->seqno = htonl(state->seqno);
	segment->ackno = htonl(state->ackno);
	segment->len = htons(20);
	segment->flags = 0;
	segment->flags |= htonl(ACK);
	segment->window = htons(state->cfg->recv_window);
 	segment->cksum = 0;
	segment->cksum = cksum(segment, sizeof(ctcp_segment_t));	// calculates the check sum 
	conn_send(state->conn, segment, sizeof(ctcp_segment_t));
	free(segment);
}


ctcp_state_t *ctcp_init(conn_t *conn, ctcp_config_t *cfg) {
  /* Connection could not be established. */
  if (conn == NULL) {
    return NULL;
  }

  /* Established a connection. Create a new state and update the linked list
     of connection states. */
  ctcp_state_t *state = calloc(sizeof(ctcp_state_t), 1);
  state->next = state_list;
  state->prev = &state_list;
  if (state_list)
    state_list->prev = &state->next;
  state_list = state;

	/* Set fields. */
	state->conn = conn;
	state->segments = ll_create();
	state->recv_segments = ll_create();
	state->queued_segments = ll_create();
	state->cfg = cfg;
	state->n_transmissions = 0;
	state->time = 0;
	state->peer_conn = true;
	state->my_conn = true;
	state->seqno = 1;
	state->ackno = 1;
	state->curr_send_window = cfg->send_window;
	state->out_seg = NULL;
  return state;
}

void ctcp_destroy(ctcp_state_t *state) {
  /* Update linked list. */
  if (state->next)
    state->next->prev = state->prev;

  *state->prev = state->next;
  conn_remove(state->conn);
	// Avoids double free corrruption
 	if(state->segments != NULL)
	{
		destroy_listObjects(state->segments);
		ll_destroy(state->segments);	
	}	
	if(state->recv_segments != NULL)
	{
		destroy_listObjects(state->recv_segments);
		ll_destroy(state->recv_segments);
	}
	if(state->queued_segments != NULL)
	{
		destroy_listObjects(state->queued_segments);
		ll_destroy(state->queued_segments);
	}
	if(state->cfg != NULL)
	{
		free(state->cfg);		
	}  
	free(state);
  end_client();
}
/**
 * State -> Void
 * Reads input from STDIN and sends it to the other host 
 *
 */
void ctcp_read(ctcp_state_t *state) {
 	// allocates memory for the buffer
	void* buffer = malloc(MAX_SEG_DATA_SIZE);
	if(buffer == NULL)
	{
		return;
	}
	// reads from STDIN
	int n_bytes = conn_input(state->conn, buffer, MAX_SEG_DATA_SIZE);
	// checks for errors 
	if(n_bytes == EOF)
	{
		// sends a FIN segment
		free(buffer);
		sendFin(state);
		state->seqno++;
		return;
	}
	
	// makes a get request if "get" was read from STDIN
	// prepares a get request if the user inputs "get"
	if(strcmp(buffer, "get\n") == 0 || strcmp(buffer, "get\r\n") == 0)
	{
		char* http_request = "GET / HTTP/1.1\r\nHost: www.google.com\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0	(X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36 OPR/47.0.2631.55\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate\r\nAccept-Language:en-US,en;q=0.8\r\n\r\n";
	n_bytes = strlen(http_request);
	strcpy(buffer, http_request);
	}
	
    // Other host can receive more data
	if(state->curr_send_window >= n_bytes)
	{
		// makes a new segment and adds it to the list of segments
		ctcp_segment_t* segment = ctcp_create_segment(state, buffer, n_bytes);
		ll_add(state->segments, segment);
		conn_send(state->conn, segment, n_bytes + sizeof(ctcp_segment_t));
		state->curr_send_window -= ntohs(segment->len); 
		state->seqno += n_bytes;
	}
	// still can receive some bytes
	else if (state->curr_send_window > 0)
	{
		// creates segments and adds them to the list 
		ctcp_segment_t* segment = ctcp_create_segment(state, buffer, state->curr_send_window); /*The data that can be sent*/
		// points to the rest of the buffer
		void* overflow_buffer = (buffer + state->curr_send_window);
		int overflow_size =  n_bytes - state->curr_send_window;
		ctcp_segment_t* overflow_segment = ctcp_create_segment(state,overflow_buffer, overflow_size); /*will be sent later*/

		ll_add(state->segments, segment);
		ll_add(state->queued_segments, overflow_segment);
		// sends the segment
		conn_send(state->conn, segment, state->curr_send_window + sizeof(ctcp_segment_t));
		state->seqno += state->curr_send_window;
		state->curr_send_window -= ntohs(segment->len); 
	}
	else
	{
		// creates the segment and adds it to the list for future transmission
		ctcp_segment_t* segment = ctcp_create_segment(state, buffer, n_bytes);
		ll_add(state->segments, segment);
	}
	free(buffer);
}
/**
  * state, segment, len -> void
  * state: is the state Object of a connection
  * segment is the segment recieved from the connection
  * len: the size of the segment including the header
  * reacts according to the recieved segment
  *
  *  Three cases:
  * - Ack packet (updates the sequence number) 
  * - FIN packet (updates the Ack number and sends ACK packet)
  * - Normal packet (updates the Ack number, sends ACK packet)
  */
void ctcp_receive(ctcp_state_t *state, ctcp_segment_t *segment, size_t len) {
  //checks to see if the segment if truncated
	if(len < ntohs(segment->len))
	{
		free(segment);
		return;			
	}

	/*validates the cksum*/
	uint16_t old_cksum = segment->cksum;
	segment->cksum = 0;
	uint16_t correct_csksum = cksum(segment, len);
	
	if(old_cksum != correct_csksum)
	{
		free(segment);
		return;
	}
		

	// checks if ACK packet was recieved
	if(ntohl(segment->flags) & ACK)
	{
		// this might be an ACK for a sent segment
		if(ll_length(state->segments) > 0)
		{
			
			ll_node_t* target_node; /* points to the node that contains the segment which the recieved acknowledgment belongs to */			
			// this was an acknowledgment for a sent segment
			if((target_node = ack_segment(state->segments, segment)) != NULL)
			{
				ctcp_segment_t* target_segment = target_node->object;

				// this was an ack for a FIN packet
				if(ntohl(target_segment->flags) & FIN)
				{
					// the other side has already terminated its connection
					if(state->peer_conn == false)
					{
						// outputs EOF 
						conn_output(state->conn, NULL, 0);
						// destroys the connection
						free(segment);
						ctcp_destroy(state);
						return;
					}
					// waites for the otherside to terminate
					else
					{
						state->my_conn = false;
					}
				}
				// an Ack for a normal packet
				else
				{
						state->curr_send_window += ntohs(target_segment->len) - sizeof(ctcp_segment_t);
						//removes the node and frees the object
						free(ll_remove(state->segments, target_node));
						state->n_transmissions = 0;
						send_queued(state);
				}
			}
		}
			// A packet with data was received
			if(ntohs(segment->len) - 20 > 0)
			{
				// puts the segment in the right order according to the seqno
				ll_node_t* prev_node;
				// examines fake packets
				if((prev_node = ctcp_prev_segment(state->recv_segments, segment)) != NULL)
				{
					ll_add_after(state->recv_segments, prev_node, segment);
					ctcp_output(state);
				}
				else if(ll_length(state->recv_segments) == 0)
				{
					ll_add(state->recv_segments, segment);
					ctcp_output(state);
				}
				else
				{
					free(segment);
				}
				return;
			}

		
	}
	// it's a FIN packet
	if(ntohl(segment->flags) & FIN)
	{
		// piggyback ACKS
		state->peer_conn = false;
		state->ackno = ntohl(segment->seqno) + 1;
		sendAck(state);
		if(state->my_conn)
		{
			sendFin(state);
			state->seqno++;
		}
		else
		{
			free(segment);
			ctcp_destroy(state);
			return;
		}	
	}
	free(segment);
}
/**
  * State -> void
  * outputs all the segments received to the state
  */
void ctcp_output(ctcp_state_t *state) {
	
 	size_t n_bytes = conn_bufspace(state->conn); /*the number of bytes available in STDOUT*/ 
	if(n_bytes <= 0)
	{
		return;
	}
	ll_node_t* curr;
	if(state->out_seg == NULL)
	{
		curr = ll_front(state->recv_segments);
	}
	else
	{
		curr = state->out_seg->next;
	}
	while(curr != NULL)
	{
		ctcp_segment_t* tmp = curr->object;
		// not enough space in STDOUT
		if(n_bytes < ntohs(tmp->len) - sizeof(ctcp_segment_t))
		{
			return;
		}
		// there is enough space
		else
		{
			state->ackno = ntohl(tmp->seqno) + ntohs(tmp->len) - sizeof(ctcp_segment_t);
			sendAck(state);
			conn_output(state->conn, tmp->data, ntohs(tmp->len) - sizeof(ctcp_segment_t));
			ll_node_t* curr_tmp = curr;
			curr = curr->next;
			state->out_seg = curr_tmp;

		}
	}
}
/**
  * Void -> Void
  * retransmits unacknowledged segments
  *
  */
void ctcp_timer() {
  // terminates on empty list  	
	if(state_list == NULL)
	{
		return;
	}
	ctcp_state_t* cursor = state_list;
	while(cursor != NULL)
	{
		//if the segment is not NULL then no ACK was received
		if(ll_length(cursor->segments) > 0)
		{
			cursor->time += cursor->cfg->timer;
			// fires only after rt_timeout seconds
			if(cursor->time >= cursor->cfg->rt_timeout)
			{
				// the segment has not reached the maximum number of transmissions
				if(cursor->n_transmissions < 4)
				{
					send_segments(cursor);
					cursor->n_transmissions++;
					cursor->time = 0;
				}
				// the other side is unresponsive
				else
				{
					ctcp_destroy(cursor);
				}
			}
		}
		cursor = cursor->next;
	}

}

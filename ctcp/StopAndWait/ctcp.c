/******************************************************************************
 * ctcp.c
 * ------
 * Implementation of cTCP done here. This is the only file you need to change.
 * Look at the following files for references and useful functions:
 *   - ctcp.h: Headers for this file.
 *   - ctcp_iinked_list.h: Linked list functions for managing a linked list.
 *   - ctcp_sys.h: Connection-related structs and functions, cTCP segment
 *                 definition.
 *   - ctcp_utils.h: Checksum computation, getting the current time.
 *
 *****************************************************************************/

#include "ctcp.h"
#include "ctcp_linked_list.h"
#include "ctcp_sys.h"
#include "ctcp_utils.h"




/**
 * Connection state.
 *
 * Stores per-connection information such as the current sequence number,
 * unacknowledged packets, etc.
 *
 * You should add to this to store other fields you might need.
 */
struct ctcp_state {
  struct ctcp_state *next;  /* Next in linked list */
  struct ctcp_state **prev; /* Prev in linked list */

  	conn_t *conn;             /* Connection object -- needed in order to figure
                           out destination when sending */
	ctcp_segment_t* segment; /*if not NULL then the segment has not been acknowledged yet*/
	ctcp_config_t* cfg; /*configuration of the connection*/
	ctcp_segment_t* recv_seg; /*last segment received from the connection associated with state*/

	int n_transmissions; // number of transmissions 
	uint32_t seqno;    // sequence number of the current connection
	uint32_t ackno;    // acknowledgement number of the current connection
	
	int time; /*the time since the last transmision of the segment*/
	
	bool peer_conn; /*The state of the other side of the connection*/
	bool my_conn; /*the state of my connection*/
};

/**
 * Linked list of connection states. Go through this in ctcp_timer() to
 * resubmit segments and tear down connections.
 */
static ctcp_state_t *state_list;

/* FIXME: Feel free to add as many helper functions as needed. Don't repeat
          code! Helper functions make the code clearer and cleaner. */

/**
  * segment -> void
  * Prints all the fields of the segment struct to stderr
  * debugging purpose
  */
void printseg(ctcp_segment_t* segment)
{
	char* flags;
	if(ntohl(segment->flags) & ACK)
	{
		if(ntohl(segment->flags) & FIN)
		{
			flags = "ACK and FIN";  
		}
		else 
		{
			flags = "ACK only";
		}
	}
	else if (ntohl(segment->flags) & FIN)
	{
		flags = "FIN only";
	}
	else
	{
		flags = "NONE";
	}
	fprintf(stderr," Sequence number: %d\n Acknowledgment number: %d\n length: %d\n flags: %s\n Window: %d\n CheckSum: %d\n\n\n",
	ntohl(segment->seqno), ntohl(segment->ackno), ntohs(segment->len), flags, ntohs(segment->window), ntohs(segment->cksum));
}

/**
  * State, flags -> void
  * given a state object, sends a FIN packet to the connection associated with the state
  *
  */
void sendFin(ctcp_state_t *state)
{
	ctcp_segment_t* segment = malloc(sizeof(ctcp_segment_t));
	segment->seqno = htonl(state->seqno);
	segment->ackno = htonl(state->ackno);
	segment->len = htons(sizeof(ctcp_segment_t));
	segment->flags = 0;
	segment->flags |= htonl(FIN);
	segment->window = htons(state->cfg->recv_window);
 	segment->cksum = 0;
	segment->cksum = cksum(segment, sizeof(ctcp_segment_t));	// calculates the check sum 
	conn_send(state->conn, segment, sizeof(ctcp_segment_t));
	if(state->segment != NULL)
	{
		free(state->segment);
	}
	state->segment = segment;
}
/**
  * State -> void
  * given a state object, sends a ACK packet to the connection associated with the state
  *
  */
void sendAck(ctcp_state_t *state)
{
	ctcp_segment_t* segment = malloc(sizeof(ctcp_segment_t));
	segment->seqno = htonl(state->seqno);
	segment->ackno = htonl(state->ackno);
	segment->len = htons(20);
	segment->flags = 0;
	segment->flags |= htonl(ACK);
	segment->window = htons(state->cfg->recv_window);
 	segment->cksum = 0;
	segment->cksum = cksum(segment, sizeof(ctcp_segment_t));	// calculates the check sum 
	conn_send(state->conn, segment, sizeof(ctcp_segment_t));

}

/**
  * conn, cfg -> ctcp_state_t 
  * given a conn (connection info) and cfg (configuration), initializes and returns a new state for the connection
  */
ctcp_state_t *ctcp_init(conn_t *conn, ctcp_config_t *cfg) {
  /* Connection could not be established. */
  if (conn == NULL) {
    return NULL;
  }

  /* Established a connection. Create a new state and update the linked list
     of connection states. */
  ctcp_state_t *state = calloc(sizeof(ctcp_state_t), 1);
  state->next = state_list;
  state->prev = &state_list;
  if (state_list)
    state_list->prev = &state->next;
  state_list = state;

  /* Set fields. */
  state->conn = conn;
  
	state->segment = NULL;
	state->cfg = cfg;
	state->ackno = 1;
	state->seqno = 1;
	state->recv_seg = NULL;
	state->time = 0;
	state->peer_conn = true;
	state->my_conn = true;
 	 return state;
}
/**
  * State -> void
  * frees all the memory associated with a connection state 
  */
void ctcp_destroy(ctcp_state_t *state) {
  /* Update linked list. */
  if (state->next)
    state->next->prev = state->prev;

  *state->prev = state->next;
  conn_remove(state->conn);

  	if(state->segment != NULL)
	{
		free(state->segment);
	}
	if(state->cfg != NULL)
	{
		free(state->cfg);		
	}
	if(state->recv_seg != NULL)
	{
		free(state->recv_seg);	
	}

  free(state);
  end_client();
}

/**
  * State -> void
  * reads an input from STDIN and sends it to the associated segment
  */
void ctcp_read(ctcp_state_t *state) {
	// the segment was not acknowldged (can't send more then a segment at a time)
	if(state->segment != NULL)
	{
		return;
	}
	// allocates memory for the buffer
	void* buffer = malloc(MAX_SEG_DATA_SIZE);
	if(buffer == NULL)
	{
		return;
	}
	// reads from STDIN
	int n_bytes = conn_input(state->conn, buffer, MAX_SEG_DATA_SIZE);
	// checks for errors 
	if(n_bytes == EOF)
	{
		// sends a FIN segment
		sendFin(state);
		return;
	}
	
	// prepares a get request if the user inputs "get"
	if(strcmp(buffer, "get\n") == 0 || strcmp(buffer, "get\r\n") == 0)
	{
		char* http_request = "GET / HTTP/1.1\r\nHost: www.google.com\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0	(X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36 OPR/47.0.2631.55\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate\r\nAccept-Language:en-US,en;q=0.8\r\n\r\n";
	n_bytes = strlen(http_request);
	strcpy(buffer, http_request);
	}
	

	/* creates a new tcp segment */
	ctcp_segment_t* segment = malloc(sizeof(ctcp_segment_t) + n_bytes);
	segment->seqno = htonl(state->seqno);
	segment->ackno = htonl(state->ackno);
	segment->len = htons(n_bytes + 20);
	segment->flags = TH_ACK; 
	segment->window = htons(state->cfg->recv_window);
 	segment->cksum = 0;
	memcpy(segment->data, buffer, n_bytes);
	segment->cksum = cksum(segment, n_bytes + sizeof(ctcp_segment_t));	// calculates the check sum 
	conn_send(state->conn, segment, n_bytes + sizeof(ctcp_segment_t));
	state->n_transmissions++;
	// sets the segment for the case of retransmissions 
	state->segment = segment;
	

}
/**
  * state, segment, len -> void
  * state: is the state Object of a connection
  * segment is the segment recieved from the connection
  * len: the size of the segment including the header
  * reacts according to the recieved segment
  *
  *  Three cases:
  * - Ack packet (updates the sequence number) 
  * - FIN packet (updates the Ack number and sends ACK packet)
  * - Normal packet (updates the Ack number, sends ACK packet)
  */
void ctcp_receive(ctcp_state_t *state, ctcp_segment_t *segment, size_t len) {
		
	//checks to see if the segment if truncated
	if(len < ntohs(segment->len))
	{
		return;			
	}
	// checks if ACK packet was recieved
	if(ntohl(segment->flags) & ACK)
	{
		// this might be an ACK for a sent segment
		if(state-> segment != NULL)
		{
			// this was an ack for a FIN packet 
			if((ntohl(state->segment->flags) & FIN) && (ntohl(state->segment->seqno) + 1 == ntohl(segment->ackno)))
			{
				// the other side has already terminated its connection
				if(state->peer_conn == false)
				{
					// outputs EOF 
					conn_output(state->conn, NULL, 0);
					// destroys the connection
					ctcp_destroy(state);
					return;
				}
				// waites for the otherside to terminate
				else
				{
					state->my_conn = false;
					state->seqno = ntohl(segment->ackno);
				}
			}
				// it's an ack for a normal packet	
			else if(ntohl(state->segment->seqno) + ntohs(state->segment->len) - sizeof(ctcp_segment_t) == ntohl(segment->ackno))
			{
				state->seqno = ntohl(segment->ackno);
				free(state->segment);
				state->segment = NULL;
				state->n_transmissions = 0;
			}
		}
		// It's a new packet
		if(ntohs(segment->len) - 20 > 0 && state->ackno == ntohl(segment->seqno))
		{
			state->ackno += ntohs(segment->len) - 20;
			state->recv_seg = segment;
			ctcp_output(state);
			
		}
	}
	// it's a FIN packet
	if(ntohl(segment->flags) & FIN)
	{
		// piggyback ACKS
		state->peer_conn = false;
		state->ackno++;
		sendAck(state);
		if(state->my_conn)
		{
			sendFin(state);
		}
		else
		{
			ctcp_destroy(state);
			return;
		}	
	}
	free(segment);
}

/*
* state -> void
* Given a state, prints the recently received segment 
* state: is the state associated with the connection
*/
void ctcp_output(ctcp_state_t *state) {
  
	size_t n_bytes = conn_bufspace(state->conn); /*the number of bytes available in STDOUT*/ 
	
	// not enough space in STDOUT
	if(n_bytes < ntohs(state->recv_seg->len) - sizeof(ctcp_segment_t))
	{
		return;
	}
	// there is enough space
	else
	{
		sendAck(state);
		conn_output(state->conn, state->recv_seg->data, ntohs(state->recv_seg->len) - sizeof(ctcp_segment_t));
		state->recv_seg = NULL;
	}
}
/**
  * Void -> Void
  * retransmits unacknowledged segments
  *
  */
void ctcp_timer() {
	
	// terminates on empty list  	
	if(state_list == NULL)
	{
		return;
	}
	ctcp_state_t* cursor = state_list;
	while(cursor != NULL)
	{
		//if the segment is not NULL then no ACK was received
		if(cursor->segment != NULL)
		{
			cursor->time += cursor->cfg->timer;
			// fires only after rt_timeout seconds
			if(cursor->time >= cursor->cfg->rt_timeout)
			{
				// the segment has not reached the maximum number of transmissions
				if(cursor->n_transmissions < 5)
				{
					conn_send(cursor->conn, cursor->segment, ntohs(cursor->segment->len) - 20 + sizeof(ctcp_segment_t));
					cursor->n_transmissions++;
					cursor->time = 0;
				}
				// the other side is unresponsive
				else
				{
					ctcp_destroy(cursor);
				}
			}
		}
		cursor = cursor->next;
	}
	
}


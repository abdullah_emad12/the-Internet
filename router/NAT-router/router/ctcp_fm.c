/**************************************************************
 * This file contains the code for a TCP finit machine object *
 * The machine contains the states and is capable of moving   *
 * from one state to the other on an input					  *
 * 															  *
 * NOTE: some of the values are hardcoded becuase I am not    *
 * expecting the state diagram to change often				  *	
 **************************************************************/
#include <stdlib.h>
#include "ctcp_fm.h"
#include <stdio.h>
/**
  * tcp_state_t* -> void
  * given a state, sets it's transitionss inputs and outputs to None and the other attributes to NULL
  */
void ctcp_fm_init_helper(tcp_state_t* state)
{	
	int i;
	for(i = 0; i < 4; i++)
	{
		state->transitions[i].input = tcp_none;
		state->transitions[i].output = tcp_none;
		state->transitions[i].next_state = NULL;
	}
}
/**
  * void -> tcp_fsm_t
  *
  * initializes all the states for finite machine for a new connection 
  * returns the start state which is "closed"
  *
  * The returned object should only be freed using ctcp_fm_destroy()
  */
tcp_fsm_t ctcp_fm_create(void)
{
	tcp_fsm_t fm; 
	
	/*start state*/
	tcp_state_t* closed = malloc(sizeof(tcp_state_t));
	fm.states[0] = closed;
	tcp_state_t* listen = malloc(sizeof(tcp_state_t));
	fm.states[1] = listen;
	tcp_state_t* syn_recieved = malloc(sizeof(tcp_state_t));
	fm.states[2] = syn_recieved;
	tcp_state_t* syn_sent = malloc(sizeof(tcp_state_t));
	fm.states[3] = syn_sent;
	tcp_state_t* established = malloc(sizeof(tcp_state_t));
	fm.states[4] = established;
	tcp_state_t* fin_wait_1 = malloc(sizeof(tcp_state_t));
	fm.states[5] = fin_wait_1;
	tcp_state_t* fin_wait_2 = malloc(sizeof(tcp_state_t));
	fm.states[6] = fin_wait_2;
	tcp_state_t* closing = malloc(sizeof(tcp_state_t));
	fm.states[7] = closing;
	tcp_state_t* time_wait = malloc(sizeof(tcp_state_t));
	fm.states[8] = time_wait;
	tcp_state_t* close_wait = malloc(sizeof(tcp_state_t));
	fm.states[9] = close_wait;
	tcp_state_t* last_ack = malloc(sizeof(tcp_state_t));
	fm.states[10] = last_ack;


	/*sets all the pointers to NULL*/
	int i;
	for(i = 0; i < TOTAL_NUMBER_STATES; i++)
	{
		ctcp_fm_init_helper(fm.states[i]);
	}


	/*transitions initializations*/
	closed->name = tcp_closed;
	closed->transitions[0].input = tcp_listen_io;
	closed->transitions[0].output = tcp_none;
	closed->transitions[0].next_state = listen;
	closed->transitions[1].input = tcp_connect; 
	closed->transitions[1].output = tcp_syn;
	closed->transitions[1].next_state = syn_sent;

	listen->name = tcp_listen;
	listen->transitions[0].input = tcp_close;
	listen->transitions[0].output = tcp_none;
	listen->transitions[0].next_state = closed;
	listen->transitions[1].input = tcp_syn;
	listen->transitions[1].output = tcp_syn_ack;
	listen->transitions[1].next_state = syn_recieved;
	listen->transitions[2].input = tcp_rst; 
	listen->transitions[2].output = tcp_none;
	listen->transitions[2].next_state = syn_recieved;
	listen->transitions[3].input = tcp_send;
	listen->transitions[3].output = tcp_syn;
	listen->transitions[3].next_state = syn_sent;

	syn_recieved->name = tcp_syn_recieved;
	syn_recieved->transitions[0].input = tcp_ack;
	syn_recieved->transitions[0].output = tcp_none;
	syn_recieved->transitions[0].next_state = established;
	syn_recieved->transitions[1].input = tcp_close;
	syn_recieved->transitions[1].output = tcp_fin;
	syn_recieved->transitions[1].next_state = fin_wait_1;
	
	syn_sent->name = tcp_syn_sent;
	syn_sent->transitions[0].input = tcp_close;
	syn_sent->transitions[0].output = tcp_none;
	syn_sent->transitions[0].next_state = closed;
	syn_sent->transitions[1].input = tcp_syn;
	syn_sent->transitions[1].output = tcp_syn_ack;
	syn_sent->transitions[1].next_state = syn_recieved;
	syn_sent->transitions[2].input = tcp_syn_ack;
	syn_sent->transitions[2].output = tcp_ack;
	syn_sent->transitions[2].next_state = established;

	established->name = tcp_established;
	established->transitions[0].input = tcp_fin;
	established->transitions[0].output = tcp_ack;
	established->transitions[0].next_state = close_wait;
	established->transitions[1].input = tcp_close;
	established->transitions[1].output = tcp_fin;
	established->transitions[1].next_state = fin_wait_1;
	established->transitions[2].input = tcp_ack;
	established->transitions[2].output = tcp_none;
	established->transitions[2].next_state = established;

	fin_wait_1->name = tcp_fin_wait_1;
	fin_wait_1->transitions[0].input = tcp_fin;
	fin_wait_1->transitions[0].output = tcp_ack;
	fin_wait_1->transitions[0].next_state = closing;
	fin_wait_1->transitions[1].input = tcp_fin_ack;
	fin_wait_1->transitions[1].output = tcp_ack;
	fin_wait_1->transitions[1].next_state = time_wait;
	fin_wait_1->transitions[2].input = tcp_ack;
	fin_wait_1->transitions[2].output = tcp_none;
	fin_wait_1->transitions[2].next_state = fin_wait_2;

	fin_wait_2->name = tcp_fin_wait_2;
	fin_wait_2->transitions[0].input = tcp_fin;
	fin_wait_2->transitions[0].output = tcp_ack;
	fin_wait_2->transitions[0].next_state = time_wait;
	
	closing->name = tcp_closing;
	closing->transitions[0].input = tcp_ack;
	closing->transitions[0].output = tcp_none;	
	closing->transitions[0].next_state = time_wait;

	time_wait->name = tcp_time_wait;
	time_wait->transitions[0].input = tcp_timeout;
	time_wait->transitions[0].output = tcp_none;
	time_wait->transitions[0].next_state = closed;

	close_wait->name = tcp_close_wait;
	close_wait->transitions[0].input = tcp_close;
	close_wait->transitions[0].output = tcp_fin;
	close_wait->transitions[0].next_state = last_ack;

	last_ack->name = tcp_last_ack;
	last_ack->transitions[0].input = tcp_ack;
	last_ack->transitions[0].output = tcp_none;
	last_ack->transitions[0].next_state = closed;
	
	return fm;
}

/** ctcp_make_transitions()
  * tcp_state_t** , tcp_io -> tcp_io   
  * Given a state machine and input, makes the transitions to the next input 
  * returns the output of the transitions
  *
  * tcp_state_t* current_state: pointer to the pointer of the current state of the machine
  * tcp_io input: input to the state machine
  */
tcp_io ctcp_make_transitions(tcp_state_t** current_state, tcp_io input)
{
	/*checks all the transitionss*/
	int i;
	for(i = 0; i < 4; i++)
	{
		if((*(current_state))->transitions[i].input == input)
		{
			tcp_io output = (*(current_state))->transitions[i].output;
			*(current_state) = 	(*(current_state))->transitions[i].next_state;
			return output;
		}
	}
	return tcp_error; /*not a Valid sequence*/
}

/** ctcp_get_input()
  * struct sr_tcp_hdr* -> tcp_io
  * given the header of a TCP segment, returns the corresponding enum tcp_io
  * struct sr_tcp_hdr* tcp_hdr: pointer to the tcp header
  *	
  */
tcp_io ctcp_get_input(struct sr_tcp_hdr* tcp_hdr)
{
	/*seperates the flag bits*/
	int fin = (tcp_hdr->ctrl_bits) & 0x1;
	int syn = (tcp_hdr->ctrl_bits >> 1) & 0x1;
	int rst = (tcp_hdr->ctrl_bits >> 2) & 0x1;
	int ack = (tcp_hdr->ctrl_bits >> 4) & 0x1;
	if(fin)
	{
		return tcp_fin;
	}
	else if(syn)
	{
		if(ack)
		{
			return tcp_syn_ack;
		}
		else
		{
			return tcp_syn;
		}
	}
	else if(ack)
	{
		return tcp_ack;
	}	
	else if(rst)
	{
		return tcp_rst;
	}
	else
	{
		return tcp_none;	
	}
}


/** ctcp_fm_destroy()
  * tcp_state_t* -> void
  * 
  * Frees all the memory associated with a finite machines
  */
void ctcp_fm_destroy(tcp_fsm_t fm)
{
	int i;
	for(i = 0; i < TOTAL_NUMBER_STATES; i++)
	{
		free(fm.states[i]);
	}
}




/*Main method to test my state machine*/
void test(void)
{
	/*create machine*/
	tcp_fsm_t fm = ctcp_fm_create();
	struct tcp_state* current_state = fm.states[0];	
	
	ctcp_make_transitions(&current_state, tcp_listen_io);
	ctcp_make_transitions(&current_state, tcp_syn);
	ctcp_make_transitions(&current_state, tcp_ack);
	ctcp_make_transitions(&current_state, tcp_close);
	ctcp_make_transitions(&current_state, tcp_fin);
	ctcp_make_transitions(&current_state, tcp_ack);
	ctcp_make_transitions(&current_state, tcp_timeout);
	ctcp_make_transitions(&current_state, tcp_connect);
	ctcp_make_transitions(&current_state, tcp_syn_ack);
	ctcp_make_transitions(&current_state, tcp_fin);
		ctcp_make_transitions(&current_state, tcp_close);
	if(current_state->name == tcp_last_ack)
	{
		printf("Success !\n");
	}
	else
	{
		printf("Failure\n");
	}
	
	
}

void print_state(tcp_state_label label)
{
	switch(label)
	{
		case tcp_closed:
			printf("closed\n");
			break;
		case tcp_listen: 
			printf("Listen\n");
			break;
		case tcp_syn_recieved: 
			printf("Syn recieved\n");
			break;
		case tcp_syn_sent: 
			printf("syn sent\n");
			break;
		case tcp_established: 
			printf("Established\n");
			break;
		case tcp_fin_wait_1: 
			printf("Fin Wait\n");
			break;
		case tcp_fin_wait_2: 
			printf("fin wait 2\n");
			break;
		case tcp_closing: 
			printf("Closing\n");
			break;
		case tcp_time_wait:
			printf("Time Wait\n");
			break;
		case tcp_close_wait: 
			printf("close Wait\n");
			break;
		case tcp_last_ack: 
			printf("last ack\n");
			break;
	}
}

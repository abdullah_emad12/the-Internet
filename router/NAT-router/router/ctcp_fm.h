
#include "sr_protocol.h"

#define TOTAL_NUMBER_STATES 11

/**
  * States for the Finite state Machine of the TCP
  */
typedef enum tcp_state_label
{
	tcp_closed,
	tcp_listen,
	tcp_syn_recieved,
	tcp_syn_sent,
	tcp_established,
	tcp_fin_wait_1,
	tcp_fin_wait_2,
	tcp_closing,
	tcp_time_wait,
	tcp_close_wait,
	tcp_last_ack
} tcp_state_label;


/**
  * inputs/outputs for the state machine
  */
typedef enum tcp_io
{
	tcp_rst,
	tcp_timeout,
	tcp_send,
	tcp_listen_io,
	tcp_close,
	tcp_connect,
	tcp_syn,
	tcp_syn_ack,
	tcp_ack,
	tcp_fin,
	tcp_fin_ack,
	tcp_none,
	tcp_error	
} tcp_io;

/*
 * a transition associated with a state
  */
typedef struct tcp_transition
{
	tcp_io input;
	tcp_io output;
	struct tcp_state* next_state;
} tcp_transition_t;


/*
a single state in the machine*/
typedef struct tcp_state
{
	tcp_state_label name;
	struct tcp_transition transitions[4]; /*list of associated transitions (at most 4 transitions)*/
} tcp_state_t;


/**
  * the instance of the state machine
  */
typedef struct tcp_fsm
{
	tcp_state_t* states[TOTAL_NUMBER_STATES];
}tcp_fsm_t;


/*
* Prototypes for public functions
*/


/**
  * void -> tcp_fsm_t
  *
  * initializes all the states for finite machine for a new connection 
  * returns the start state which is "closed"
  *
  * The returned object should only be freed using ctcp_fm_destroy()
  */
tcp_fsm_t ctcp_fm_create(void);


/** ctcp_make_transitions()
  * tcp_state_t** , tcp_io -> tcp_io   
  * Given a state machine and input, makes the transitions to the next input 
  * returns the output of the transitions
  *
  * tcp_state_t* current_state: pointer to the pointer of the current state of the machine
  * tcp_io input: input to the state machine
  */
tcp_io ctcp_make_transitions(tcp_state_t** current_state, tcp_io input);


/** ctcp_get_input()
  * struct sr_tcp_hdr* -> tcp_io
  * given the header of a TCP segment, returns the corresponding enum tcp_io
  * struct sr_tcp_hdr* tcp_hdr: pointer to the tcp header
  *	
  */
tcp_io ctcp_get_input(struct sr_tcp_hdr* tcp_hdr);


/** ctcp_fm_destroy()
  * tcp_state_t* -> void
  * 
  * Frees all the memory associated with a finite machines
  */
void ctcp_fm_destroy(tcp_fsm_t fm);

tcp_io ctcp_make_transitions(tcp_state_t** current_state, tcp_io input);


void print_state(tcp_state_label label);




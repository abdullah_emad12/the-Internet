
#include <signal.h>
#include <assert.h>
#include "sr_nat.h"
#include "sr_utils.h"
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/*Prototypes (private functions)*/
void sr_nat_timeout_conns(struct sr_nat* nat, struct sr_nat_mapping* mapping);
struct sr_nat_connection* sr_nat_tcp_conn_lookup(struct sr_nat_connection* conns, uint32_t dst_ip, uint16_t dst_port);
void sr_nat_time_update_tcp(struct sr_nat *nat, struct sr_nat_mapping* mapping, uint32_t dst_ip, uint16_t dst_port);
void sr_timeout_nat_mappings(struct sr_nat *nat);
void sr_timeout_nat_cache(struct sr_nat *nat);
void sr_timeout_inbound_syns(struct sr_nat *nat);
struct sr_nat_mapping *sr_nat_get_original_mapping(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type); 



int sr_nat_init(struct sr_nat *nat, struct sr_instance* sr) { /* Initializes the nat */

  assert(nat);

  /* Acquire mutex lock */
  pthread_mutexattr_init(&(nat->attr));
  pthread_mutexattr_settype(&(nat->attr), PTHREAD_MUTEX_RECURSIVE);
  int success = pthread_mutex_init(&(nat->lock), &(nat->attr));

  /* Initialize timeout thread */

  pthread_attr_init(&(nat->thread_attr));
  pthread_attr_setdetachstate(&(nat->thread_attr), PTHREAD_CREATE_JOINABLE);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_create(&(nat->thread), &(nat->thread_attr), sr_nat_timeout, nat);

  /* CAREFUL MODIFYING CODE ABOVE THIS LINE! */

  nat->mappings = NULL;
  nat->cache = NULL;
  nat->in_syn = ll_create();
  nat->tcp_machine = ctcp_fm_create();
  nat->sr = sr;
  /* Initialize any variables here */


  return success;
}


int sr_nat_destroy(struct sr_nat *nat) {  /* Destroys the nat (free memory) */

  pthread_mutex_lock(&(nat->lock));

  /* free nat memory here */
	ctcp_fm_destroy(nat->tcp_machine);
	ll_destroy(nat->in_syn);
  pthread_kill(nat->thread, SIGKILL);
  return pthread_mutex_destroy(&(nat->lock)) &&
    pthread_mutexattr_destroy(&(nat->attr));

}
/** void sr_nat_mapping_destroy()
  * struct sr_nat_mapping*, struct sr_nat_mapping*, struct sr_nat* -> struct sr_nat_mapping* 
  *
  * Removes an element from a Linked list given the node to be deleted and the node pointing to it
  * Careful using this. May ruin the linked list if the node given was not pointing to the target node
  * 
  * returns the element that should be pointed at instead of the destroyed node.i.e: the node that was pointing to the destroyed node. 
  *
  * struct sr_nat* nat: the instance of the NAT
  * struct sr_nat_mapping* prev_node: the node pointing to the target node to be deleted
  * struct sr_nat_mapping* target_node: the node to be deleted
  *
  * if prev_node = NULL, the target node is assumed to be the first node in the list
  */
struct sr_nat_mapping* sr_nat_mapping_destroy(struct sr_nat_mapping** head, struct sr_nat_mapping* prev_node, struct sr_nat_mapping* target_node)
{
	/*target Node cannot be NULL*/
	if(target_node == NULL)
	{
		return NULL;
	}
	/*first element in the list*/
	if(prev_node == NULL)
	{
		(* head) = target_node->next;
		target_node->next = NULL;
		free(target_node);
		return (* head);
	}
	/*In the middle of the list*/
	else
	{
		prev_node->next = target_node->next;
		target_node->next = NULL;
		free(target_node);
		return prev_node;
	}
}


/** sr_cache_mapping()
  *
  * struct sr_nat*, mapping -> void
  *
  * adds a copy of the mapping to the cache. This function will be usually used before destroying a mapping in
  * case you wish the NAT to reuse this mapping for the upcoming connections that has the same internal (ip, aux)
  * as the cached mapping
  * 
  * struct sr_nat* nat: instance of the NAT
  * struct mapping* mapping: old mapping
  *
  */
void sr_cache_mapping(struct sr_nat* nat, struct sr_nat_mapping* mapping)
{
	struct sr_nat_mapping* mapcpy = malloc(sizeof(struct sr_nat_mapping));
	memcpy(mapcpy, mapping, sizeof(struct sr_nat_mapping));
	mapcpy->next = NULL;

	mapcpy->next = nat->cache;
	nat->cache = mapcpy;
}

void *sr_nat_timeout(void *nat_ptr) {  /* Periodic Timout handling */
  struct sr_nat *nat = (struct sr_nat *)nat_ptr;
  while (1) {
    sleep(1.0);
    pthread_mutex_lock(&(nat->lock));

    /* handle periodic tasks here */
	sr_timeout_nat_mappings(nat);
	sr_timeout_nat_cache(nat);
	sr_timeout_inbound_syns(nat);

/*TODO: timeout SYNS*/
    pthread_mutex_unlock(&(nat->lock));

  }
  return NULL;
}

/*==============================================================================================================
* Timeout helper private functions
*/

/** sr_timeout_nat_mappings()
  * struct sr_nat *nat -> void
  * 
  * Given a nat object, frees all the defunct mapping according to the configured timeout of each mapping type
  * 
  * struct sr_nat *nat: the nat instance
  */
void sr_timeout_nat_mappings(struct sr_nat *nat)
{
	time_t current_time = time(NULL);
	struct sr_nat_mapping* mapping_cursor = nat->mappings;
	struct sr_nat_mapping* prev_mapping =  NULL;
	/*iterates over all the mappings*/
	while(mapping_cursor != NULL)
	{	
		switch(mapping_cursor->type)
		{	
			/*the mapping was to an ICMP packet*/
			case nat_mapping_icmp:
				/*Inactive for 60 seconds*/
				if(current_time - mapping_cursor->last_updated >= nat->i_timeout)
				{
					/*puts the mapping in the cache to be reused*/
					sr_cache_mapping(nat, mapping_cursor);
					mapping_cursor = sr_nat_mapping_destroy(&(nat->mappings), prev_mapping, mapping_cursor);
					printf("\nICMP mapping Cleared\n");
				}
				break;

			/*the Mapping was to a TCP packet*/
			case nat_mapping_tcp:
			/*Gives the NAT 10 seconds to initialize new connections (if any)*/
			if(current_time - mapping_cursor->last_updated >= 1)
			{
				sr_nat_timeout_conns(nat, mapping_cursor);
				/*checks the state of the connection*/
				if(mapping_cursor->conns == NULL)
				{
					sr_cache_mapping(nat, mapping_cursor);
					mapping_cursor = sr_nat_mapping_destroy(&(nat->mappings), prev_mapping, mapping_cursor);
					printf("\nTCP mapping Cleared\n");
				}
			}
			break;
		}
		prev_mapping = mapping_cursor;
		if(mapping_cursor != NULL)
		{
			mapping_cursor = mapping_cursor->next;
		}

	}
}


/** sr_timeout_nat_cache()
  * struct sr_nat *nat -> void
  * 
  * Given a nat object, Frees all the old mappings that has been in the cache for CACHE_TIMEOUT seconds
  * 
  * struct sr_nat *nat: the nat instance
  */
void sr_timeout_nat_cache(struct sr_nat *nat)
{
	time_t current_time = time(NULL);
	struct sr_nat_mapping* mapping_cursor = nat->cache;
	struct sr_nat_mapping* prev_mapping =  NULL;
	/*iterates over all the mappings*/
	while(mapping_cursor != NULL)
	{	
		/*It has been sitting in the cache for CACHE_TIMEOUT seconds */
		if(current_time - mapping_cursor->last_updated >= CACHE_TIMEOUT)
		{
			mapping_cursor = sr_nat_mapping_destroy(&(nat->cache), prev_mapping, mapping_cursor);
			printf("\ncache timedout!\n");
		}	
		prev_mapping = mapping_cursor;
		if(mapping_cursor != NULL)
		{
			mapping_cursor = mapping_cursor->next;
		}
	}
}

/** sr_timeout_inbound_syns()
  * struct sr_nat *nat -> void
  * 
  * Given a nat object checks for all the unsolicited inbound SYN packets, 
  * if a corresponding was found, silently drops, otherwise, if it has been sitting
  * in memory for 6 seconds drop and send ICMP port unreachable (type 3, code 3)
  *
  * struct sr_nat *nat: the nat instance
  */
void sr_timeout_inbound_syns(struct sr_nat *nat)
{
	time_t current_time = time(NULL);
	struct ll_node* ll_cursor = nat->in_syn->head;
	while(ll_cursor != NULL)
	{
		struct sr_pending_syn* syn_strct = (struct sr_pending_syn*)ll_cursor->object;
		struct sr_nat_mapping* mapping = sr_nat_lookup_external(nat, syn_strct->dst_port, nat_mapping_icmp);
		/*Does not have mapping*/
		if(mapping == NULL)
		{
			/*It has been here already for 6 seconds, sends ICMP*/
			if(current_time - syn_strct->recieved_at >= SYN_TIMEOUT)
			{
				struct sr_if* interface_ext = sr_get_interface(nat->sr, INTERFACE_EXT);
				/*creates icmp, places it in an ip packet than ships it to the destination*/
				uint8_t* pkt = NULL;
				int len = sr_generate_icmp(&(pkt), icmp_type_dest_unreachable, 3, 0 ,0, syn_strct->pkt, ICMP_DATA_SIZE); 
				if(len == 0) return;
				len = sr_generate_ip(&(pkt), pkt, len, interface_ext->ip, syn_strct->src_ip);
				if(len == 0) return;
		
				/*LPM*/
				struct sr_rt* nexthop_rt = sr_longest_prefix_match(nat->sr, syn_strct->src_ip);	
				sr_send_eth_packet(nat->sr, pkt , len,nexthop_rt, ethertype_ip);
				
				/*removes syn object from list and Frees all the associated memory*/
				struct ll_node* tmp = ll_cursor;
				ll_cursor = ll_cursor->prev;
				struct sr_pending_syn* syn_destroy = ll_remove(nat->in_syn, tmp);
				free(syn_destroy);
				
				
			}
		}
		/*Drop silently*/
		else
		{
			/*removes syn object from list and Frees all the associated memory*/
			struct ll_node* tmp = ll_cursor;
			ll_cursor = ll_cursor->prev;
			struct sr_pending_syn* syn_destroy = ll_remove(nat->in_syn, tmp);
			free(syn_destroy);
			free(mapping);
		}
		/*The removed element was in the middle*/
		if(ll_cursor != NULL)
		{
			ll_cursor = ll_cursor->next;
		}
		/*The removed element was the old head*/
		else
		{
			ll_cursor = nat->in_syn->head;
		}
	}
}

/**
  * timeouts the connections in a conn list for a given mapping
  */
void sr_nat_timeout_conns(struct sr_nat* nat, struct sr_nat_mapping* mapping)
{
	time_t current_time = time(NULL);
	struct sr_nat_connection* conn_cursor = mapping->conns;
	struct sr_nat_connection* conn_prev = NULL;
	int i = 0;
	while(conn_cursor != NULL)
	{
		/*A connection was in the established state*/
		bool clear = false;
		if(conn_cursor->current_state->name == tcp_established)
		{
			if(current_time - conn_cursor->last_updated >= nat->e_timeout)
			{
				clear = true;
			}
		}
		else if(conn_cursor->current_state->name == tcp_closed || conn_cursor->current_state->name == tcp_time_wait)
		{
			clear = true;
		}
		else
		{
			if(current_time - conn_cursor->last_updated >= nat->r_timeout)
			{
				clear = true;
			}
		}
		/*the clear flag is set (DELETE ITEM)*/
		if(clear)
		{
			/*first Item*/
			if(mapping->conns == conn_cursor)	
			{
				mapping->conns = conn_cursor->next;
				free(conn_cursor);
				conn_cursor = mapping->conns; 
			}	
			else
			{
				conn_prev->next = conn_cursor->next;
				free(conn_cursor);
				conn_cursor = conn_prev->next;
			}
		}
		else
		{
			conn_prev = conn_cursor;
			conn_cursor = conn_cursor->next;
		}
		i++;
	}
}


/* Get the mapping associated with given external port.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_external(struct sr_nat *nat,
    uint16_t aux_ext, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));

  /* handle lookup here, malloc and assign to copy */
  struct sr_nat_mapping *copy = NULL;
  
	/*Iterates over the NAT mappings list*/
	struct sr_nat_mapping* map_cursor = nat->mappings;
	while(map_cursor != NULL)
	{
		/*found a corresponding mapping*/
		if(map_cursor->aux_ext == aux_ext && map_cursor->type == type)
		{
			/*Updates the time*/
			map_cursor->last_updated = time(NULL);
			copy = malloc(sizeof(struct sr_nat_mapping));
			memcpy(copy, map_cursor,sizeof(struct sr_nat_mapping));
			break;
		}
		map_cursor = map_cursor->next;
	}
	
  pthread_mutex_unlock(&(nat->lock));
  return copy;
}

/* Get the mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_internal(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));

  /* handle lookup here, malloc and assign to copy. */
  struct sr_nat_mapping *copy = NULL;

	/*Iterates over the NAT mappings list*/
	struct sr_nat_mapping* map_cursor = nat->mappings;
	while(map_cursor != NULL)
	{
		/*found a corresponding mapping*/
		if(map_cursor->aux_int == aux_int && map_cursor->ip_int == ip_int && map_cursor->type == type)
		{
			copy = malloc(sizeof(struct sr_nat_mapping));
			map_cursor->last_updated = time(NULL);
			memcpy(copy, map_cursor,sizeof(struct sr_nat_mapping));
			break;	
		}
		map_cursor = map_cursor->next;
	}
  pthread_mutex_unlock(&(nat->lock));
  return copy;
}



/* Get the cached mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL.
	NOTE: the returned mapping is removed from the cache list */
struct sr_nat_mapping *sr_cache_lookup_internal(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));

  /* handle lookup here, malloc and assign to copy. */
  struct sr_nat_mapping* ret = NULL;

	/*Iterates over the NAT mappings list*/
	struct sr_nat_mapping* map_cursor = nat->cache;
	struct sr_nat_mapping* prev = NULL;
	while(map_cursor != NULL)
	{
		/*found a corresponding mapping*/
		if(map_cursor->aux_int == aux_int && map_cursor->ip_int == ip_int && map_cursor->type == type)
		{
			/*removeing from the head of the list*/
			if(prev == NULL)
			{
				nat->cache = map_cursor->next;
				map_cursor->next = NULL;	
			}
			/*in the middle or at the tail*/
			else
			{
				prev->next = map_cursor->next;
				map_cursor->next = NULL;
			}
			ret = map_cursor;
			ret->last_updated = time(NULL);
			break;	
		}
		prev = map_cursor;
		map_cursor = map_cursor->next;
	}

  	pthread_mutex_unlock(&(nat->lock));
  	return ret;
}



/* Insert a new mapping into the nat's mapping table.
   Actually returns a copy to the new mapping, for thread safety.
 */
struct sr_nat_mapping *sr_nat_insert_mapping(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  	pthread_mutex_lock(&(nat->lock));

  /* handle insert here, create a mapping, and then return a copy of it */
  	struct sr_nat_mapping *mapping = NULL;


	/*Looks for a mapping in the cache*/
	mapping =  sr_nat_lookup_internal(nat, ip_int, aux_int, type);
	/*The mapping already exists (return copy of the map)*/
	if(mapping != NULL)
	{
		/*Creates a copy for the new Mapping*/
		pthread_mutex_unlock(&(nat->lock));
		return mapping;
	}



	/*Search for recently removed mapping in the NAT cache*/
	mapping =  sr_cache_lookup_internal(nat, ip_int, aux_int, type);
	if(mapping == NULL)
	{
		/*Creates a new mapping*/
		mapping = malloc(sizeof(struct sr_nat_mapping));
		/*Makes its best to avoid segmentation faults*/
		if(mapping != NULL)
		{
			struct sr_if* interface_ext = sr_get_interface(nat->sr, INTERFACE_EXT);
			/*initializes the mapping values*/
			mapping->ip_int = ip_int;
			mapping->ip_ext = interface_ext->ip;
			mapping->aux_int = aux_int;
			mapping->aux_ext = 1024;
			mapping->ip_ext = interface_ext->ip;
			mapping->type = type;
			mapping->conns = NULL;
			mapping->next = NULL;
			mapping->last_updated = time(NULL);
		}	
		
	}

	/*inserts the mapping in the List and make sure it has unique aux_ext*/
	/*the map list is empty*/	
	if(nat->mappings == NULL)
	{
		nat->mappings = mapping;
	}
	else 
	{
		/*Iterates over the NAT mappings list to get the last element*/
		struct sr_nat_mapping* map_cursor = nat->mappings;
		struct sr_nat_mapping* last_mapping = nat->mappings;
		while(map_cursor != NULL)
		{
			/*Uses an External AUX that has not been used before for this type of mapping*/
			if((htons(map_cursor->aux_ext) ==  mapping->aux_ext) && (map_cursor->type ==  mapping->type))
			{
				/*Makes some effort to ensure the mapping is less randomized and unique*/
				mapping->aux_ext += aux_int;
			}
			last_mapping = map_cursor;
			map_cursor = map_cursor->next;
		}
		last_mapping->next = mapping;
	}
	mapping->aux_ext = htons(mapping->aux_ext);

	/*Creates a copy for the new Mapping*/
	struct sr_nat_mapping* mapping_cpy  = malloc(sizeof(struct sr_nat_mapping));
	if(mapping_cpy == NULL)
	{
		/*avoids possible double free corruption*/
		if(mapping != NULL)
		{
			free(mapping);
		}
	}
	else
	{
		memcpy(mapping_cpy, mapping, sizeof(struct sr_nat_mapping));
	}




 	pthread_mutex_unlock(&(nat->lock));
 	return mapping_cpy;
}

/*
* Inserts a new Connection State into a TCP mapping
* the given conn onject shouldn't be freed 
*/
void sr_nat_insert_conn(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int, uint32_t dst_ip, uint16_t dst_port)
{
	
	 pthread_mutex_lock(&(nat->lock));
	/*Looks up for a TCP mapping for the given internal (ip,port) tuple*/
	struct sr_nat_mapping* mapping = sr_nat_get_original_mapping(nat, ip_int, aux_int, nat_mapping_tcp);
	if(mapping == NULL)
	{
		pthread_mutex_unlock(&(nat->lock));
		return;
	}
	/*First element in the list*/
	if(mapping->conns == NULL)
	{
		struct sr_nat_connection* conn = sr_nat_initialize_conn(nat, dst_ip, dst_port);
		ctcp_make_transitions(&(conn->current_state), tcp_connect);
		mapping->conns = conn;
	}
	else
	{
		/*Gets the last element in the list*/
		struct sr_nat_connection* conn_cursor = mapping->conns;
		while(conn_cursor != NULL)
		{
			if(conn_cursor->dst_ip == dst_ip && conn_cursor->dst_port == dst_port)
			{
				/*resets the current state to syn sent*/
				conn_cursor->current_state = nat->tcp_machine.states[0];
				ctcp_make_transitions(&(conn_cursor->current_state), tcp_connect);
				break;
			}
			/*Last element in the list*/
			if(conn_cursor->next == NULL)
			{
					struct sr_nat_connection* conn = sr_nat_initialize_conn(nat, dst_ip, dst_port);
					ctcp_make_transitions(&(conn->current_state), tcp_connect);
					conn_cursor->next = conn;
					break;
			}
			conn_cursor = conn_cursor->next;
		}
	}
	
	pthread_mutex_unlock(&(nat->lock));
}

/**
  * changes the TCP state of a given TCP connection
  * 
  * struct sr_nat *nat: the nat instance
  * uint32_t ip_int: the internal ip address of the private device
  * uint16_t aux_int: the internal port 
  * uint32_t dst_ip: the ip adress of the other end-point 
  * uint16_t dst_port: the port of the other end-point
  *
  */
tcp_io sr_nat_change_tcp_state(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int,
							uint32_t dst_ip, uint16_t dst_port, tcp_io state_in)
{
	 pthread_mutex_lock(&(nat->lock));

	tcp_io output = tcp_error;
	/*Looks up for a TCP mapping for the given internal (ip,port) tuple*/
	struct sr_nat_mapping* mapping = sr_nat_get_original_mapping(nat, ip_int, aux_int, nat_mapping_tcp);
	if(mapping != NULL)
	{
		/*Modifies the previous state of a conn that matches the destination ip and port*/
		struct sr_nat_connection* conn = sr_nat_tcp_conn_lookup(mapping->conns, dst_ip, dst_port);
		if(conn != NULL)
		{
			output = ctcp_make_transitions(&(conn->current_state), state_in);
			conn->last_updated = time(NULL);
		}
	}
	
	pthread_mutex_unlock(&(nat->lock));
	return output;
}

/**
  * initializes a new conn object
  */
struct sr_nat_connection* sr_nat_initialize_conn(struct sr_nat *nat, uint32_t dst_ip, uint16_t dst_port)
{
	struct sr_nat_connection* conn = malloc(sizeof(struct sr_nat_connection));
	conn->current_state = nat->tcp_machine.states[0];
	conn->dst_ip = dst_ip;
	conn->dst_port = dst_port;
	conn->current_state = nat->tcp_machine.states[0]; 
	conn->last_updated = time(NULL);
	conn->next = NULL;
	return conn; 
}

/*
* looks up a conn in a list of conns 
* important note: should not be used outside the functions that modifies the actual connection states in the NAT
*/
struct sr_nat_connection* sr_nat_tcp_conn_lookup(struct sr_nat_connection* conns, uint32_t dst_ip, uint16_t dst_port)
{
	while(conns != NULL)
	{
		/*A match was found*/
		if(conns->dst_ip == dst_ip && conns->dst_port == dst_port)
		{
			return conns;
		}
		conns = conns->next;
	}
	return NULL;
}





/**
  * updates the time in a give Mapping of a TCP
  * Note: should not be used outside the functions that deals with the actual mappings
  */
void sr_nat_time_update_tcp(struct sr_nat *nat, struct sr_nat_mapping* mapping, uint32_t dst_ip, uint16_t dst_port)
{
	time_t current_time = time(NULL);
	
	if(mapping == NULL)
	{
		return;
	}
	
	/*updates the last_updated to the current time*/
	mapping->last_updated = current_time;

	/*updates the given connection*/
	struct sr_nat_connection* conn = sr_nat_tcp_conn_lookup(mapping->conns, dst_ip, dst_port);
	conn->last_updated = current_time;

}
/*
* updates the time for an ICMP mapping
*/
void sr_nat_time_update_icmp(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int)
{
	time_t current_time = time(NULL);
	pthread_mutex_lock(&(nat->lock));
	
	/*Looks up for a TCP mapping for the given internal (ip,port) tuple*/
	struct sr_nat_mapping* mapping = sr_nat_lookup_internal(nat, ip_int, aux_int, nat_mapping_icmp);
	if(mapping == NULL)
	{
		return;
	}
	
	/*updates the last_updated to the current time*/
	mapping->last_updated = current_time;


	pthread_mutex_unlock(&(nat->lock));
}
/*====================================================================================================================*/
/** sr_nat_get_original_mapping()
  * struct sr_nat, uint32_t, uint16_t, sr_nat_mapping_type -> struct sr_nat_mapping
  * 
  * Returns the Original mapping for a given (ip, port) tuple
  * Warning: should never be used outside a function that does not use mutext locks
  * 
  * struct sr_nat *nat: nat object
  * uint32_t ip_int: internal ip mapping
  * uint16_t aux_int: internal port mapping
  * sr_nat_mapping_type type: mapping type
  */

struct sr_nat_mapping *sr_nat_get_original_mapping(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

	/*Iterates over the NAT mappings list*/
	struct sr_nat_mapping* map_cursor = nat->mappings;
	while(map_cursor != NULL)
	{
		/*found a corresponding mapping*/
		if(map_cursor->aux_int == aux_int && map_cursor->ip_int == ip_int && map_cursor->type == type)
		{
			return map_cursor;
		}
		map_cursor = map_cursor->next;
	}
  return NULL;
}

/**
  * struct sr_nat* -> void
  * prints the content of the mapping list (for debugging purposes)
  * 
  * struct sr_nat* nat
  */
void sr_print_mapping(struct sr_nat* nat)
{
	struct sr_nat_mapping* nat_ptr = nat->mappings;
	if(nat_ptr == NULL)
	{
		printf("NULL");
	}
	while(nat_ptr != NULL)
	{
		printf("Internal IP:\n");
		print_addr_ip_int(nat_ptr->ip_int);
		printf("Internal Port: %d\nExternal Port: %d", ntohs(nat_ptr->aux_int), ntohs(nat_ptr->aux_ext));	
		nat_ptr = nat_ptr->next;	
	}
}

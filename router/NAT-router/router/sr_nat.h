
#ifndef SR_NAT_TABLE_H
#define SR_NAT_TABLE_H

#include <inttypes.h>
#include <time.h>
#include <pthread.h>
#include "sr_linked_list.h"
#include "ctcp_fm.h"
#include "sr_router.h"


/*Constants*/
#define INTERFACE_INT "eth1" /*internal interface*/
#define INTERFACE_EXT "eth2" /*external interface*/
#define CACHE_TIMEOUT 20 /*timeout of an old mapping*/
#define SYN_TIMEOUT 6 /*time before responding to unsolicited inbound syn*/

typedef enum {
  nat_mapping_icmp,
  nat_mapping_tcp
  /* nat_mapping_udp, */
} sr_nat_mapping_type;



/* unsolicited inbound SYN */
struct sr_pending_syn
{
	uint32_t src_ip; /*IP of the sender*/
	uint16_t src_port; /*source port of the sender*/
	uint16_t dst_port; /*External Port of the reciever*/
	uint8_t pkt[ICMP_DATA_SIZE]; /*a copy of the first 28 bytes of the original packet*/
	time_t recieved_at; /*the time at which the SYN was received*/
};

struct sr_nat_connection {
  /* add TCP connection state data members here */
	uint32_t dst_ip;
	uint16_t dst_port;
	struct tcp_state* current_state; 

	time_t last_updated; /* use to timeout mappings */

  struct sr_nat_connection *next;
};

struct sr_nat_mapping {
  sr_nat_mapping_type type;
  uint32_t ip_int; /* internal ip addr */
  uint32_t ip_ext; /* external ip addr */
  uint16_t aux_int; /* internal port or icmp id */
  uint16_t aux_ext; /* external port or icmp id */
  time_t last_updated; /* use to timeout mappings */
  struct sr_nat_connection *conns; /* list of connections. null for ICMP */
  struct sr_nat_mapping *next;
};

struct sr_nat {
	/* add any fields here */
	struct sr_nat_mapping *mappings; /*All the active NAT Mapping*/

	struct sr_nat_mapping* cache; /*saves the defunct mappings for some time,
								 in an effort to stabelize the map assignment process*/

	struct linked_list * in_syn;  /* unsolicited inbound SYNs List */

	struct tcp_fsm tcp_machine; /*the Finite state machine of the TCP 
								(one machine will be shared among all connections)*/
	struct sr_instance* sr; /*instance of the router*/

	time_t i_timeout; /*ICMP timeout*/
	time_t e_timeout; /*Established TCP timeout*/
	time_t r_timeout; /*Transistory TCP timeout*/
		
	/* threading */
	pthread_mutex_t lock;
	pthread_mutexattr_t attr;
	pthread_attr_t thread_attr;
	pthread_t thread;
};


int sr_nat_init(struct sr_nat *nat, struct sr_instance* sr); /* Initializes the nat */
int   sr_nat_destroy(struct sr_nat *nat);  /* Destroys the nat (free memory) */
void *sr_nat_timeout(void *nat_ptr);  /* Periodic Timout */

/* Get the mapping associated with given external port.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_external(struct sr_nat *nat,
    uint16_t aux_ext, sr_nat_mapping_type type );

/* Get the mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_internal(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type );


/* Get the cached mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_cache_lookup_internal(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type );

/* Insert a new mapping into the nat's mapping table.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_insert_mapping(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type );

/*
* Inserts a new Connection State into a TCP mapping
* the given conn onject shouldn't be freed 
*/
void sr_nat_insert_conn(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int, uint32_t dst_ip, uint16_t dst_port);


/*
* updates the time for an ICMP mapping
*/
void sr_nat_time_update_icmp(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int);


/**
  * changes the TCP state of a given TCP connection
  */
tcp_io sr_nat_change_tcp_state(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int,
							uint32_t dst_ip, uint16_t dst_port, tcp_io state_in);


/**
  * initializes a new conn object
  */
struct sr_nat_connection* sr_nat_initialize_conn(struct sr_nat *nat, uint32_t dst_ip, uint16_t dst_port);


void sr_print_mapping(struct sr_nat* nat);


#endif

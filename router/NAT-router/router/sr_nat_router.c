#include <signal.h>
#include <assert.h>
#include "sr_nat.h"
#include "sr_router.h"
#include "sr_utils.h"
#include "sr_nat_router.h"
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

/*********************************************************************************
*																				 *
* NAT packets handling functions												 *
* 																				 *	
**********************************************************************************/

/*prototypes*/
int sr_process_received_tcp_segment(struct sr_instance* sr, struct sr_ip_hdr* ip_hdr);
int sr_process_received_icmp_segment(struct sr_instance* sr, struct sr_ip_hdr* ip_hdr);
struct sr_pending_syn* sr_create_pending_syn(uint32_t src_ip, uint16_t src_port, uint16_t dst_port, uint8_t* ip_pkt);

int sr_nat_mask_tcp(struct sr_instance* sr, uint8_t* ip_pkt);
int sr_nat_unmask_tcp(struct sr_instance* sr, uint8_t* ip_pkt);
int sr_nat_mask_icmp(struct sr_instance* sr, uint8_t* ip_pkt);
int sr_nat_unmask_icmp(struct sr_instance* sr, uint8_t* ip_pkt);
void update_tcp_checksum(uint8_t* pkt);

/** sr_handle_nat_packet
  *  sr_instance, uint8_t*, size_t -> int
  *  Forwards a given packet according to the NAT specifications
  *  Triggered only if the NAT is enabled
  *	 
  *  return value: return zero if the packet should be forwarded, if 1 most probably an ICMP error message will 
  *  be sent by the router. If 2, the router calling function should ignore this packet. 
  *  Error code 3: indicates that the router should drop the packet and send ICMP error message to the sender
  * 
  *  sr_instance sr: the instance of the router
  *  uint8_t* ip_pkt: a pointer to the ip packet (borrowed)
  *  size_t len: the length of the ip packet
  *  char* recv_interface: the interface at which the ip packet was recieved
  */
int sr_nat_handle_packet(struct sr_instance* sr, uint8_t* ip_pkt, size_t len, const char* recv_interface)
{
	struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*) ip_pkt;
	

	/*Is it received From an internal interface ?*/
	if(strcmp(recv_interface, INTERFACE_INT) == 0)
	{
		/*Is it a TCP segment?*/
		if(ip_protocol(ip_pkt) == ip_protocol_tcp)
		{
			return sr_nat_mask_tcp(sr, ip_pkt);
		}
		/*Is it an ICMP message ?*/
		else if(ip_protocol(ip_pkt) == ip_protocol_icmp)
		{
			return sr_nat_mask_icmp(sr, ip_pkt);
		}
		/*drop the packet*/
		else
		{
			printf("stopped here!\n\n");
			return 1;
		}
		
	}
	/*received on External interface*/
	else
	{
		/*returns destination NET unreachable incase the IP address was not destined to the external interface*/
		struct sr_if* interface =  sr_get_interface(sr, INTERFACE_EXT);
		if(interface->ip != ip_hdr->ip_dst)
		{
			return 3;
		}

		/*Is it a TCP segment ? */
		if(ip_protocol(ip_pkt) == ip_protocol_tcp)
		{
			return sr_process_received_tcp_segment(sr, ip_hdr);
		}
		/*ICMP*/
		else if(ip_protocol(ip_pkt) == ip_protocol_icmp)
		{
			return sr_process_received_icmp_segment(sr, ip_hdr);
		}
		
	}
	return 0;
}

/**sr_process_received_tcp_segment
  * struct sr_instance* sr, struct sr_ip_hdr* ip_hdr
  *
  * process a TCP segment and decides whether to drop it or forward it
  * The TCP segment is assumed to be received on the external interface
  * struct sr_instance* sr: sr instance of the router 
  *	struct sr_ip_hdr* ip_hdr: ip header of the received packet
  *
  */
int sr_process_received_tcp_segment(struct sr_instance* sr, struct sr_ip_hdr* ip_hdr)
{
		uint8_t* ip_pkt = (uint8_t*) ip_hdr;
		struct sr_tcp_hdr* tcp_hdr = (struct sr_tcp_hdr*) (ip_pkt + sizeof(struct sr_ip_hdr));
		int ret_code = 0;

		/*Is it a SYN packet ?*/
		int syn  = ((tcp_hdr->ctrl_bits) >> 1) & 0x1;
		/*syn packet*/
		if(syn)
		{
			/*is There a mapping corresponding to the destination? */
			struct sr_nat_mapping* mapping = sr_nat_lookup_external(sr->nat, tcp_hdr->dst_port , nat_mapping_tcp);
			/*unsolicited inbound SYN*/
			if(mapping == NULL)
			{
				/*initialize and adds a SYN object*/
				struct sr_pending_syn* syn_obj = sr_create_pending_syn(ip_hdr->ip_src, tcp_hdr->src_port, tcp_hdr->dst_port, (uint8_t*)ip_hdr);
				ll_add(sr->nat->in_syn, syn_obj);
				ret_code = 2;
			}
			else
			{
 				ret_code =  sr_nat_unmask_tcp(sr, ip_pkt);
				free(mapping);
			}
		}
		/*other segment*/
		else
		{
			/*is There a mapping corresponding to the destination? */
			struct sr_nat_mapping* mapping = sr_nat_lookup_external(sr->nat, tcp_hdr->dst_port , nat_mapping_tcp);
			if(mapping == NULL)
			{
				return 1;
			}
			else
			{
				
 				ret_code =  sr_nat_unmask_tcp(sr, ip_pkt);
				free(mapping);
			}
		}
		
	return ret_code;
}


/**sr_process_received_icmp_packet
  * struct sr_instance* sr, struct sr_ip_hdr* ip_hdr
  *
  * process a icmp packet and decides whether to drop it or forward it
  * The ICM packet is assumed to be received on the external interface
  * returns zero to forward and 1 to drop 
  *
  * struct sr_instance* sr: sr instance of the router 
  *	struct sr_ip_hdr* ip_hdr: ip header of the received packet
  *
  */
int sr_process_received_icmp_segment(struct sr_instance* sr, struct sr_ip_hdr* ip_hdr)
{
	uint8_t* ip_pkt = (uint8_t*) ip_hdr;
	
	struct sr_icmp_t3_hdr* icmp_hdr = (struct sr_icmp_t3_hdr*) (ip_pkt + sizeof(struct sr_ip_hdr));

	/*is it and echo reply?*/
    if(icmp_hdr->icmp_type == icmp_type_request)
	{
		return 0;
	}
	else if(icmp_hdr->icmp_type == icmp_type_reply)
	{
		/*does it have a mapping?*/
		struct sr_nat_mapping* mapping = sr_nat_lookup_external(sr->nat, icmp_hdr->unused , nat_mapping_icmp);
		if(mapping == NULL)
		{
			return 1;
		}
		else
		{
			free(mapping);
			return sr_nat_unmask_icmp(sr, ip_pkt);
		}

	}
	else 
	{
		return 1;
	}
	
}

/*=============================================================================================================================================
packet Masking functions */

/** 
  * sr_nat_mask_tcp()
  * struct sr_instance*, uint8_t* -> int
  * Given an ip packet that contains a TCP segment, changes the ip adress and port in the packet,
  * and creates mapping and corresponding connection if needed.
  *
  * returns zero if the packet should be forwarder and one if it should be dropped 
  *
  * struct sr_instance* sr: the instance of the nat
  * uint8_t*: pointer to ip packet
  *
  * Note: packets passed to this function are assumed to be received from the internal interface
  */

 int sr_nat_mask_tcp(struct sr_instance* sr, uint8_t* ip_pkt)
{
	struct sr_nat *nat = sr->nat;
	struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*) ip_pkt;
	struct sr_tcp_hdr* tcp_hdr = (struct sr_tcp_hdr*) (ip_pkt + sizeof(struct sr_ip_hdr));

	/*get mapping*/
	struct sr_nat_mapping* mapping = sr_nat_lookup_internal(nat, ip_hdr->ip_src, tcp_hdr->src_port, nat_mapping_tcp); 

	/*No mapping was found*/
	if(mapping == NULL)
	{
		mapping = sr_nat_insert_mapping(nat, ip_hdr->ip_src, tcp_hdr->src_port, nat_mapping_tcp);
		if(mapping == NULL)
		{
			/*couldn't create mapping (drop packet)*/
			return 1;
		}
	}
	
	tcp_io packet_type = ctcp_get_input(tcp_hdr);

	/*SYN packet*/
	if(packet_type == tcp_syn)
	{
		/*creates and inserts new connection in the mapping (if one does not exist)*/
		sr_nat_insert_conn(nat, mapping->ip_int, mapping->aux_int, ip_hdr->ip_dst, tcp_hdr->dst_port);
	}
	else
	{
		/*make the transition according to the packet's control bits*/
		if(packet_type == tcp_fin)
		{
			sr_nat_change_tcp_state(nat, ip_hdr->ip_src, tcp_hdr->src_port, ip_hdr->ip_dst, tcp_hdr->dst_port, tcp_close);	
		}
		else if(tcp_fin_ack)
		{
			sr_nat_change_tcp_state(nat, ip_hdr->ip_src, tcp_hdr->src_port, ip_hdr->ip_dst, tcp_hdr->dst_port, tcp_close);
			sr_nat_change_tcp_state(nat, ip_hdr->ip_src, tcp_hdr->src_port, ip_hdr->ip_dst, tcp_hdr->dst_port, tcp_ack);		
		}
		else
		{
			tcp_io in = ctcp_get_input(tcp_hdr);
			sr_nat_change_tcp_state(nat, ip_hdr->ip_src, tcp_hdr->src_port, ip_hdr->ip_dst, tcp_hdr->dst_port, in);	
		}
	}
	/*mask the packet and update the checksum*/
	ip_hdr->ip_src = mapping->ip_ext;
	ip_hdr->ip_sum = 0;
	ip_hdr->ip_sum = cksum(ip_hdr, sizeof(struct sr_ip_hdr));


	tcp_hdr->src_port = mapping->aux_ext;
	update_tcp_checksum(ip_pkt);
	
	free(mapping);
	return 0;

/*TODO: make sure the insert function adds the external ip to any mapping*/
	
}


/** 
  * sr_nat_unmask_tcp()
  * struct sr_instance*, uint8_t* -> int
  * Given an ip packet that contains a TCP segment, Changes the destination of the of the IP packet 
  * according to the Nat Mapping or send port unreachable if no mapping was found
  *
  * returns zero if the packet should be forwarder and one if it should be dropped and ICMP error message should be send 
  * return value of 2 indicates that the calling function should terminate.
  * 
  * struct sr_instance* sr: the instance of the nat
  * uint8_t*: pointer to ip packet
  *
  * Note: packets passed to this function are assumed to be received from the external interface
  */

 int sr_nat_unmask_tcp(struct sr_instance* sr, uint8_t* ip_pkt)
{
	struct sr_nat *nat = sr->nat;
	struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*) ip_pkt;
	struct sr_tcp_hdr* tcp_hdr = (struct sr_tcp_hdr*) (ip_pkt + sizeof(struct sr_ip_hdr));

	/*Looks for mapping in the cache*/	
	struct sr_nat_mapping* mapping = sr_nat_lookup_external(nat, tcp_hdr->dst_port,nat_mapping_tcp);
	
	/*Mapping was found*/
	if(mapping != NULL)
	{
		tcp_io tcp_input = ctcp_get_input(tcp_hdr);
		sr_nat_change_tcp_state(nat, mapping->ip_int, mapping->aux_int, ip_hdr->ip_src, tcp_hdr->src_port, tcp_input);
		/*invalid sequence of TCP segments*/		
	
		/*unmask the packt*/
		tcp_hdr->dst_port = mapping->aux_int;
		ip_hdr->ip_dst = mapping->ip_int;

			
		/*updates the checksums*/
		ip_hdr->ip_sum = 0;
		ip_hdr->ip_sum = cksum(ip_hdr, sizeof(struct sr_ip_hdr));
		
		update_tcp_checksum(ip_pkt);
		free(mapping);
			
	}
	else
	{
		return 1;
	}
	return 0;
}
/** sr_nat_mask_icmp()
  * strcut sr_instance*, uint8_t* -> int
  *
  * Given an ICMP packet From the internal interface, Creates new mapping (if one did not exist)
  * and changes the source ip and identifier according to the mapping
  *
  * return 0 if the packet should be forwarded and 1 if it should be dropped
  *
  * struct sr_instance* sr: the instance of the nat
  * uint8_t*: pointer to ip packet
  * 
  * Note: the passed ICMP packet is assumed to be coming from the inernal interface
  */
int sr_nat_mask_icmp(struct sr_instance* sr, uint8_t* ip_pkt)
{
	struct sr_nat *nat = sr->nat;
	struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*) ip_pkt;
	struct sr_icmp_t3_hdr* icmp_hdr = (struct sr_icmp_t3_hdr*) (ip_pkt + sizeof(struct sr_ip_hdr));

	/*inserts a mapping if one didn't exist*/	
	struct sr_nat_mapping* mapping = sr_nat_insert_mapping(nat, ip_hdr->ip_src, icmp_hdr->unused, nat_mapping_icmp);

	ip_hdr->ip_src = mapping->ip_ext;
	icmp_hdr->unused = mapping->aux_ext;
	
	/*recomputes the checksum*/
	ip_hdr->ip_sum = 0;
	ip_hdr->ip_sum = cksum(ip_hdr, sizeof(struct sr_ip_hdr));

	icmp_hdr->icmp_sum = 0;
	icmp_hdr->icmp_sum = cksum(icmp_hdr, (ntohs(ip_hdr->ip_len) - sizeof(struct sr_ip_hdr)));
	free(mapping);
	return 0;
}

/** sr_nat_unmask_icmp()
  * strcut sr_instance*, uint8_t* -> int
  *
  * Given an ICMP packet From the external interface, looks for the mapping 
  * that correspondce to the Identifier and unmasks the packet
  *
  * return 0 if the packet should be forwarded and 1 if it should be dropped
  *
  * struct sr_instance* sr: the instance of the nat
  * uint8_t*: pointer to ip packet
  * 
  * Note: the passed ICMP packet is assumed to be coming from the external interface
  */
int sr_nat_unmask_icmp(struct sr_instance* sr, uint8_t* ip_pkt)
{
	struct sr_nat *nat = sr->nat;
	struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*) ip_pkt;
	struct sr_icmp_t3_hdr* icmp_hdr = (struct sr_icmp_t3_hdr*) (ip_pkt + sizeof(struct sr_ip_hdr));
	struct sr_nat_mapping* mapping = sr_nat_lookup_external(nat, icmp_hdr->unused ,nat_mapping_icmp);
	
	/*mapping was not found*/
	if(mapping == NULL)
	{
		return 3;
	}

	ip_hdr->ip_dst = mapping->ip_int;
	icmp_hdr->unused = mapping->aux_int;

	/*recomputes the checksum*/
	ip_hdr->ip_sum = 0;
	ip_hdr->ip_sum = cksum(ip_hdr, sizeof(struct sr_ip_hdr));

	icmp_hdr->icmp_sum = 0;
	icmp_hdr->icmp_sum = cksum(icmp_hdr, (ntohs(ip_hdr->ip_len) - sizeof(struct sr_ip_hdr)));
	free(mapping);
	return 0;
	
}

/*========================================================================================================================================*/
/**
  *	sr_create_pending_syn
  * uint32_t, uint16_t, uint16_t -> struct sr_pending_syn *
  * given the source ip, source port and destination port creates and return a struct sr_pending_syn* for
  * the receieved packet
  *
  */
struct sr_pending_syn* sr_create_pending_syn(uint32_t src_ip, uint16_t src_port, uint16_t dst_port, uint8_t* ip_pkt)
{
	struct sr_pending_syn* syn_obj = malloc(sizeof(struct sr_pending_syn));
	if(syn_obj == NULL)
	{
		return NULL;
	}
	syn_obj->src_ip = src_ip;
	syn_obj->src_port = src_port;
	syn_obj->dst_port = dst_port;
	syn_obj->recieved_at = time(NULL);
	memcpy(syn_obj->pkt, ip_pkt, ICMP_DATA_SIZE);		
	return syn_obj;
}

/** update_tcp_checksum()
  * uint8_t* -> void
  * given the whole ip packet that contains a TCP segment, calculates the the checksum of the TCP header and updates it
  *
  * uint8_t* pkt: pointer to the ip packet
  */
void update_tcp_checksum(uint8_t* pkt)
{
	struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*) pkt;
	struct sr_tcp_hdr* tcp_hdr = (struct sr_tcp_hdr*) (pkt + sizeof(struct sr_ip_hdr));
	tcp_hdr->cksum = 0;
	int tcp_length = ntohs(ip_hdr->ip_len) - sizeof(struct sr_ip_hdr);
    /*Creates the psuedo packet*/
	struct sr_psuedo_hdr* psuedo_hdr = malloc(sizeof(struct sr_psuedo_hdr) + tcp_length);
	/*couldn't allocate memory*/
	if(psuedo_hdr == NULL)
	{
		return;
	}
	/*copies the necessary information to the psuedoheader*/
	psuedo_hdr->ip_dst = ip_hdr->ip_dst;
	psuedo_hdr->ip_src = ip_hdr->ip_src;
	psuedo_hdr->reserved = 0;
	psuedo_hdr->ip_protocol = ip_protocol_tcp;
	psuedo_hdr->tcp_length = htons(tcp_length);
	memcpy(psuedo_hdr->data, tcp_hdr, tcp_length);
	/*updates the checksum*/
	tcp_hdr->cksum = cksum(psuedo_hdr, sizeof(struct sr_psuedo_hdr) + tcp_length);
	tcp_hdr = (struct sr_tcp_hdr*) (((uint8_t*) psuedo_hdr) + sizeof(struct sr_psuedo_hdr));
	
	/*frees temporary memory*/
	free(psuedo_hdr);
}


/** sr_handle_nat_packet
  *  sr_instance, uint8_t*, size_t -> int
  *  Forwards a given packet according to the NAT specifications
  *  Triggered only if the NAT is enabled
  *	 
  *  return value: return zero if the packet should be forwarded, otherwise the packet should be dropped
  * 
  *  sr_instance sr: the instance of the router
  *  uint8_t* ip_pkt: a pointer to the ip packet (borrowed)
  *  size_t len: the length of the ip packet
  *  char* recv_interface: the interface at which the ip packet was recieved
  */
int sr_nat_handle_packet(struct sr_instance* sr, uint8_t* ip_pkt, size_t len, const char* recv_interface);


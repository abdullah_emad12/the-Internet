/*-----------------------------------------------------------------------------
 * File: sr_router.h
 * Date: ?
 * Authors: Guido Apenzeller, Martin Casado, Virkam V.
 * Contact: casado@stanford.edu
 *
 *---------------------------------------------------------------------------*/

#ifndef SR_ROUTER_H
#define SR_ROUTER_H

#include <netinet/in.h>
#include <sys/time.h>
#include <stdio.h>

#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_nat.h"

/* we dont like this debug , but what to do for varargs ? */
#ifdef _DEBUG_
#define Debug(x, args...) printf(x, ## args)
#define DebugMAC(x) \
  do { int ivyl; for(ivyl=0; ivyl<5; ivyl++) printf("%02x:", \
  (unsigned char)(x[ivyl])); printf("%02x",(unsigned char)(x[5])); } while (0)
#else
#define Debug(x, args...) do{}while(0)
#define DebugMAC(x) do{}while(0)
#endif

#define INIT_TTL 255
#define PACKET_DUMP_SIZE 1024

/* forward declare */
struct sr_if;
struct sr_rt;

/* ----------------------------------------------------------------------------
 * struct sr_instance
 *
 * Encapsulation of the state for a single virtual router.
 *
 * -------------------------------------------------------------------------- */

struct sr_instance
{
    int  sockfd;   /* socket to server */
    char user[32]; /* user name */
    char host[32]; /* host name */ 
    char template[30]; /* template name if any */
    unsigned short topo_id;
    struct sockaddr_in sr_addr; /* address to server */
    struct sr_if* if_list; /* list of interfaces */
    struct sr_rt* routing_table; /* routing table */
    struct sr_arpcache cache;   /* ARP cache */
	struct sr_nat* nat; /*NAT instance*/
    pthread_attr_t attr;
    FILE* logfile;
};




/* -- sr_main.c -- */
int sr_verify_routing_table(struct sr_instance* sr);

/* -- sr_vns_comm.c -- */
int sr_send_packet(struct sr_instance* , uint8_t* , unsigned int , const char*);
int sr_connect_to_server(struct sr_instance* ,unsigned short , char* );
int sr_read_from_server(struct sr_instance* );



/* -- sr_if.c -- */
void sr_add_interface(struct sr_instance* , const char* );
void sr_set_ether_ip(struct sr_instance* , uint32_t );
void sr_set_ether_addr(struct sr_instance* , const unsigned char* );
void sr_print_if_list(struct sr_instance* );

/* -- sr_router.c -- */
void sr_init(struct sr_instance* );
void sr_handlepacket(struct sr_instance* , uint8_t * , unsigned int , char* );
void sr_broadcast_arp_request(struct sr_instance* sr, struct sr_arpreq* arpreq);
unsigned int sr_broadcast_destination_host_unreach(struct sr_instance* sr, struct sr_packet* lst_pkt);
struct sr_rt* sr_longest_prefix_match(struct sr_instance* sr, uint32_t ip);

/** sr_generate_eth_packet
  * 
  * uint8_t**, uint8_t*, size_t, char[], char[], uint16_t -> size_t
  *
  * Generates an ehternet packet with given payload and sets the given pointer to the generated packet 
  * returns the length of the packet or 0 on error
  *
  * uint8_t** eth_pkt: pointer to the pointer that will point to the ethernet packet
  * uint8_t* data: the payload of the ethernet packet (should be freed)
  * size_t data_len: length of the payload
  * char [] src_mac: source mac address
  * char [] dst_mac: destination mac address 
  * uint16_t eth_type: type of the payload 
  *
  * Note: All header info must be in network byte-order
  */
unsigned int sr_generate_eth_packet(uint8_t** eth_pkt, uint8_t* data, size_t data_len,
							 unsigned char src_mac [], unsigned char dst_mac[],  uint16_t eth_type);


/**
  * uint8_t**, uint8_t*, size_t uint32_t, uint32_t -> int
  *
  * Creates an IP packet with a source and destination ip adresses, then places a given data with a given length as
  * payload for the ip packet. It should free the given payload after copying it to the ip packet 
  * 
  * returns the total size of the ip packet including the payload; len(ip_header) + len(payload)
  * or 0 on error
  *
  * uint8_t** pkt_ptr: pointer to a pointer that will be pointing to the ip packet 
  * uint8_t* payload: payload of the ip packet 
  * size_t  payload_len: length of the payload in bytes
  * uint32_t ip_src: source ip adress of the sender (Network Byte-Order)
  * uint32_t ip_dst: destination ip adress of the receiver (Network-Order)
  *	
  */
unsigned int sr_generate_ip(uint8_t** pkt_ptr, uint8_t* payload, size_t payload_len, uint32_t ip_src, uint32_t ip_dst);


/**
  * uint8_t**, uint8_t, uint8_t, uint16_t, uint16_t -> uint8_t
  * generates an icmp packet given the type and the code of the icmp message
  * the value of the icmp packet pointer is placed in pkt_ptr*
  * returns a the length of the packet or 0 on error
  * 
  * uint8_t** pkt_ptr: pointer to the the pointer of the icmp packet
  * uint8_t type: type of the icmp message (host byte-order)
  * uint8_t code: code of the icmp message (host byte-order)
  * uint16_t identifier: the identifier of an echo reply (Network byte-order)
  * uint16_t sequence: the sequence number of an echo request (Network byte-order)
  * The identifier and the sequence number maybe set to zero in case of icmp message other than echo reply
  *  uint8_t* original_packet: Pointer to the original packet assoicated with this ICMP message
  */
unsigned int sr_generate_icmp(uint8_t** pkt_ptr, uint8_t type, uint8_t code,
							  uint16_t identifier, uint16_t seqno,
							  uint8_t* original_pkt, size_t original_len);

/**
  * struct sr_instance*, uint8_t*, size_t, sr_if*, uint32_t, uint16_t-> int
  * 
  * Creates an Ethernet Packet with a given data as a payload, then sends it to the appropriate interface
  * returns the length of the packet on success or an error code on failure 
  * 
  * Error code: 
  * General Failure: -1
  * Longest Prefix match: -2
  *
  * struct sr_instance* sr: instance of the sr router
  * uint8_t* data: pointer to the payload data
  * size_t data_len: length of the payload in bytes
  * sr_if* src_if: the interface struct of the sender
  * uint32_t dst_ip: the destination ip address of the receiver
  * uint16_t ether_type: the type of the data enclosed in the ethernet frame
  *
  */ 
int sr_send_eth_packet(struct sr_instance* sr, uint8_t* data, size_t data_len, struct sr_rt*  nexthop_rt, uint16_t ether_type);	


#endif /* SR_ROUTER_H */

#include "../sr_nat.h"
#include "../sr_router.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

/*=============================== tests*/

/*compares two mappings*/
int map_cmp(struct sr_nat_mapping* mapping1, struct sr_nat_mapping* mapping2)
{
	if(mapping1->ip_int != mapping2->ip_int || mapping1->ip_ext != mapping2->ip_ext || mapping1->aux_int != mapping2->aux_int || mapping1->aux_ext != mapping2->aux_ext || mapping1->type != mapping2->type)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
int test_nat(struct sr_instance* sr)
{
	
	/*TODO: test all the functions in the sr_nat.c*/
	/*inserts random mapping into the NAT*/
	srand(time(NULL)); 

	uint32_t ip = rand() % 1600000;
	uint16_t port = rand() % 1500;
	   
	struct sr_nat_mapping* mapping1 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_tcp);
	struct sr_nat_mapping* mapping2 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_tcp);

	if(map_cmp(mapping1, mapping2) != 0)
	{
		printf("Test Fails. Expecting the two mappings to be the same, but found different mappings\n");
		return 1;
	}
	struct sr_nat_mapping* mapping3 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_icmp);
	if(map_cmp(mapping1, mapping3) == 0)	
	{
		printf("Test Fails. Expecting the two mappings to be different, but they are the same\n");
		return 1;
	}
	/*checks the mapping contains exactly two mapping and the corresponds to the expected mappings*/
	int i = 0;	
	struct sr_nat_mapping* cursor = sr->nat->mappings;
	while(cursor != NULL)
	{
		i++;
		cursor = cursor->next;
	}
	if(i != 2)
	{
		printf("Test Fails. Expecting to find two mappings\n");
		return 1;
	}

		/*tests the cache*/
	struct sr_nat_mapping* tmp = sr->nat->mappings;
	sr->nat->mappings = sr->nat->mappings->next;
	sr->nat->cache = tmp;
	
	struct sr_nat_mapping* mapping4 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_tcp);

	if(map_cmp(mapping1, mapping4) != 0)	
	{
		printf("Test Fails. Expecting the two mappings to be the same\n");
		return 1;
	}


	ip = rand() % 1600000;
	port = rand() % 1500;
	struct sr_nat_mapping* mapping12 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_tcp);
	
	ip = rand() % 1600000;
	port = rand() % 1500;
	struct sr_nat_mapping* mapping13 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_icmp);

	ip = rand() % 1600000;
	port = rand() % 1500;
	struct sr_nat_mapping* mapping14 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_icmp);


	ip = rand() % 1600000;
	port = rand() % 1500;
	struct sr_nat_mapping* mapping15 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_tcp);


	ip = rand() % 1600000;
	port = rand() % 1500;
	struct sr_nat_mapping* mapping16 = sr_nat_insert_mapping(sr->nat, ip, port, nat_mapping_tcp);



	/*test look up functions*/
	struct sr_nat_mapping* mapping_res1 = sr_nat_lookup_external(sr->nat,
   mapping13->ip_ext, nat_mapping_icmp);

	if(map_cmp(mapping_res1, mapping13) != 0)	
	{
		printf("Test Fails. Expecting the two mappings to be the same\n");
		return 1;
	}

	struct sr_nat_mapping* mapping_res2 = sr_nat_lookup_internal(sr->nat,mapping14->ip_int, mapping14->aux_int, nat_mapping_tcp);

	if(map_cmp(mapping_res2, mapping14) != 0)	
	{
		printf("Test Fails. Expecting the two mappings to be the same\n");
		return 1;
	}
	struct sr_nat_connection* conn =  sr_nat_initialize_conn(sr->nat, ip, port);
	sr_nat_insert_conn(sr->nat, mapping14->ip_int, mapping14->aux_int, conn);

	struct sr_nat_mapping* mapping_res3 = sr_nat_lookup_external(sr->nat,
   mapping14->ip_ext, nat_mapping_tcp);
	
	if(mapping_res3 == NULL)
	{
		printf("couldn't inserts a connection\n");
		return 1;
	}

	printf("All tests ran successfully!\n");
	free(mapping1);
	free(mapping2);
	free(mapping3);
	free(mapping4);
	free(mapping12);
	free(mapping13);
	free(mapping14);
	free(mapping15);
	free(mapping16);
	free(mapping_res1);
	free(mapping_res2);
	free(mapping_res3);

	return 0;

	
}

/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

/*Global Variables*/
uint16_t ip_identification = 0;


/*Prototypes*/
int sr_process_ip_segment(struct sr_instance* sr, uint8_t* packet, unsigned int len);
int sr_process_arp_segment(struct sr_instance* sr, uint8_t* packet,  unsigned int len);

int sr_send_eth_packet(struct sr_instance* sr, uint8_t* data, size_t data_len, struct sr_rt*  nexthop_rt, uint16_t ether_type);
int sr_forward_packet(struct sr_instance* sr, uint8_t* ip_pkt, size_t len);
unsigned int sr_send_all_arp_queue(struct sr_instance* sr, struct sr_arpreq* arpreq);

unsigned int sr_generate_icmp(uint8_t** pkt_ptr, uint8_t type, uint8_t code, uint16_t identifier, uint16_t seqno, uint8_t* original_pkt, size_t original_len);
unsigned int sr_generate_arp_request(uint8_t** arp_ptr, struct sr_if* interface, uint32_t dst_ip);
unsigned int sr_generate_ip(uint8_t** pkt_ptr, uint8_t* payload, size_t payload_len, uint32_t ip_src, uint32_t ip_dst);
unsigned int sr_generate_eth_packet(uint8_t** eth_pkt, uint8_t* data, size_t data_len,
							unsigned char src_mac[], unsigned char dst_mac[],  uint16_t eth_type);
unsigned int sr_generate_arp_reply(uint8_t** arp_ptr, struct sr_if* interface, uint32_t dst_ip, unsigned char dst_mac[]);

struct sr_rt* sr_longest_prefix_match(struct sr_instance* sr, uint32_t ip);
sr_icmp_hdr_t sr_resolve_icmp_hdr(uint16_t* icmp);

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
    
    /* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */)
{
  /* REQUIRES */
  	assert(sr);
  	assert(packet);
  	assert(interface);

  	printf("*** -> Received packet of length %d \n",len);
	

	/*Drops the packet if it was too small*/
	if(len < 36)
	{
		printf("Error: Packet is too small. Possibly corrupted or Truncated\n");
		return;
	}
	 int err;
	
	/*Process the packet according to it's type*/
	switch(ethertype(packet))
	{
		/*Process IP packet and return on error*/
		case ethertype_ip:
			err = sr_process_ip_segment(sr, packet, len);
			if(err)
			{
				printf("Error: IP packet fields are incorrect\n");
				return;
			}	
		break;

		/*Process ARP packet and return on error*/
		case ethertype_arp:
			err = sr_process_arp_segment(sr, packet, len);
			if(err)
			{
				printf("Error: ARP packet fields are incorrect\n");
			}
		break;	
		/*Drops the packet if its neither IP nor ARP*/
		default:
			printf("Error: Ethernet packet type unkonw. Dropping packet....");
		break;
	}
	printf("interface: %s", interface);
	
}/* end sr_ForwardPacket */




/**
  * struct sr_instance*, uint8_t*, int -> void 
  * Given a pointer to the whole ethernet frame process the ip packet and takes necessary actions
  * returns 0 on success, and 1 otherwise 
  *
  * struct sr_instance* sr: the sr instance of the router
  * uint8_t* packet: the whole ethernet Frame (borrowed)
  * int len: length of the whole frame
  *
  */

int sr_process_ip_segment(struct sr_instance* sr, uint8_t* packet, unsigned int len)
{
	sr_ip_hdr_t* ip_header = (sr_ip_hdr_t*) (packet + sizeof(sr_ethernet_hdr_t));
	unsigned int ip_len = len - sizeof(sr_ethernet_hdr_t);
 	
	/*
	 Check for the validity of the packet */
	
	/*checks ip version is 4*/
	if(ip_header->ip_v != 0x4)
	{
		return 1;
	}

	/*checks the length of the packet*/
	if(ntohs(ip_header->ip_len) != ip_len)
	{
		return 1;
	}

	/*computes the checksum and compares it to the one in the fields*/
	uint16_t sum_temp = ip_header->ip_sum;
	ip_header->ip_sum = 0;

	int ip_correct_cksum = cksum(ip_header, sizeof(sr_ip_hdr_t));
	if(ip_correct_cksum != sum_temp)
	{
		return 1;
	}
	 ip_header->ip_sum = sum_temp;
	
	
	/*Maps the Mac address from the received packet to the ip address in the ARP cache*/
	struct sr_ethernet_hdr* tmp_eth = (struct sr_ethernet_hdr*) packet;
	sr_arpcache_insert(&(sr->cache), tmp_eth->ether_shost , ip_header->ip_src);

	

	/*checks if the ip belongs to one of our interfaces*/
	struct sr_if* interface = sr_if_find(sr->if_list, ip_header->ip_dst);
	
	


	/*Not destined here*/
	if(interface == NULL)
	{	
		/*creates a copy from ip packet and forwards it*/
		uint8_t* packet_cpy = malloc(ip_len);
		memcpy(packet_cpy, (uint8_t*)ip_header, ip_len);
		sr_forward_packet(sr,packet_cpy, ip_len);
		return 0;
	}
	
	/*This was an icmp packet*/
	if(ip_protocol((uint8_t*)ip_header) == ip_protocol_icmp)
	{

		
		unsigned int icmp_len =  ip_len - sizeof(sr_ip_hdr_t);
		uint8_t* icmp_packet_tmp =  (((uint8_t*) ip_header) + sizeof(sr_ip_hdr_t));/*temporary pointer*/		

		/* extracts the icmp header fields in the appropraite byte order*/ 
		sr_icmp_hdr_t icmp_hdr = sr_resolve_icmp_hdr((uint16_t*)icmp_packet_tmp);
		
		/*checks the icmp for corruption*/
		uint16_t old_sum = icmp_hdr.icmp_sum;
		((sr_icmp_t3_hdr_t*)icmp_packet_tmp)->icmp_sum = 0;
		int icmp_correct_cksum = cksum(icmp_packet_tmp , icmp_len);
		if(old_sum != icmp_correct_cksum)
		{
			return 1;
		}
		((sr_icmp_t3_hdr_t*)icmp_packet_tmp)->icmp_sum = old_sum;
		/* this was an ICMP echo request*/		
		if(icmp_hdr.icmp_type == icmp_type_request && icmp_hdr.icmp_code == 0)
		{
			uint8_t* pkt = NULL;
			/*to extract the identifier and the sequence number*/
			struct sr_icmp_echo_reply* tmp = (struct sr_icmp_echo_reply*) icmp_packet_tmp; 
			
			/*In order to copy the old payload into the new one*/
			uint8_t* icmp_payload = icmp_packet_tmp + sizeof(struct sr_icmp_echo_reply);
			size_t icmp_payload_len  = icmp_len - sizeof(struct sr_icmp_echo_reply);
			/*creates and icmp and places it in an ip packet then sends*/
			unsigned int len = sr_generate_icmp(&(pkt), icmp_type_reply, 0, tmp->echo_identifier, tmp->echo_seqno,icmp_payload, icmp_payload_len);
			if(len == 0) return 1;

			len = sr_generate_ip(&(pkt), pkt, len, interface->ip, ip_header->ip_src);
			if(len == 0) return 1;
			
			/*LPM*/
			struct sr_rt* nexthop_rt = sr_longest_prefix_match(sr, ip_header->ip_src);	
			/*pkt now points to an ip with icmp as payload and len contains the total length of the ip packet*/
			int err = sr_send_eth_packet(sr, pkt ,len , nexthop_rt,  ethertype_ip);
			if(err)
			{
				return 1;
			}
		}
	}
	/*this was UDP or TCP packet*/
	else if(ip_protocol((uint8_t*)ip_header) == ip_protocol_tcp || ip_protocol((uint8_t*)ip_header) == ip_protocol_udp)
	{
		/*creates icmp, places it in an ip packet than ships it to the destination*/
		uint8_t* pkt = NULL;
		int len = sr_generate_icmp(&(pkt), icmp_type_dest_unreachable, 3, 0 ,0,(uint8_t*) ip_header, ip_len); 
		if(len == 0) return 1;
		len = sr_generate_ip(&(pkt), pkt, len, interface->ip, ip_header->ip_src);
		if(len == 0) return 1;
		
		/*LPM*/
		struct sr_rt* nexthop_rt = sr_longest_prefix_match(sr, ip_header->ip_src);	
		int err = sr_send_eth_packet(sr, pkt , len,nexthop_rt, ethertype_ip);
		if(err)
		{
			return 1;
		}
		
	}
	
	return 0;
	
}

/** sr_process_arp_segment
  *
  * struct sr_instance*, uint8_t*, unsigned int -> void 
  * Given an ARP packet, process this packet and respond accodingly
  * return 0 if the processing was sucessful and 1 otherwise
  *
  * struct sr_instance* sr: instance of the router
  * uint8_t* packet*: the whole ethernet Frame (borrowed)
  * unsigned int len: length of the packet
  *
  */ 
int sr_process_arp_segment(struct sr_instance* sr, uint8_t* packet,  unsigned int len)
{
	/*reads the content of the header*/
	sr_arp_hdr_t* arp_hdr = (sr_arp_hdr_t*) (packet + sizeof(struct sr_ethernet_hdr));
	int arp_len = len - sizeof(struct sr_ethernet_hdr);
	
	/*the packet size is not acceptable*/
	if(arp_len < sizeof(sr_arp_hdr_t))
	{
		return 1;
	}

	/*protocol must be ip*/
	if(ntohs(arp_hdr->ar_pro) != ethertype_ip)
	{
		return 1;
	}
	/*checks if the ip belongs to one of our interfaces*/
	struct sr_if* interface = sr_if_find(sr->if_list, arp_hdr->ar_tip);

	if(interface != NULL)
	{
		/*resolves the op code*/
		switch(ntohs(arp_hdr->ar_op))
		{
			case arp_op_request:
			{
				/*sends a reply*/
				uint8_t* pkt = NULL;
				unsigned int pkt_len = sr_generate_arp_reply(&(pkt), interface, arp_hdr->ar_sip, arp_hdr->ar_sha);

				/*makes sure to insert the adress in the ARP cache*/
				sr_arpcache_insert(&(sr->cache), arp_hdr->ar_sha, arp_hdr->ar_sip);

				/*sends the packet and checks for errors */ 
				struct sr_rt* nexthop_rt = sr_longest_prefix_match(sr, arp_hdr->ar_sip);					
				int err =  sr_send_eth_packet(sr, pkt, pkt_len, nexthop_rt, ethertype_arp);
				if(err) return 1;

				break;
			}
			case arp_op_reply: 
			{
				/*ip->mac map*/
				struct sr_arpreq* arp_req = sr_arpcache_insert(&(sr->cache), arp_hdr->ar_sha, arp_hdr->ar_sip);
				if(arp_req == NULL)
				{
					return 2;
				}				
				sr_send_all_arp_queue(sr, arp_req);
			
				/*removes the arp request from the queue*/
				sr_arpreq_destroy(&(sr->cache) , arp_req);

				break;			
			}
			default: 
			{
				return 1;
			}
		}
	}
	return 0;
}

/**
  * struct sr_instance*, uint8_t*, size_t, sr_if*, uint32_t, uint16_t-> int
  * 
  * Creates an Ethernet Packet with a given data as a payload, then sends it to the appropriate interface
  * returns the length of the packet on success or an error code on failure 
  * 
  * Error code: 
  * General Failure: -1
  * Longest Prefix match: -2
  *
  * struct sr_instance* sr: instance of the sr router
  * uint8_t* data: pointer to the payload data
  * size_t data_len: length of the payload in bytes
  * sr_if* src_if: the interface struct of the sender
  * uint32_t dst_ip: the destination ip address of the receiver
  * uint16_t ether_type: the type of the data enclosed in the ethernet frame
  *
  */ 
int sr_send_eth_packet(struct sr_instance* sr, uint8_t* data, size_t data_len, struct sr_rt*  nexthop_rt, uint16_t ether_type)
{
	/*cannot have NULL data as payload*/
	if(data == NULL)
	{
		return -1;
	}

	/*Looks up the next hop IP address in the ARP cache*/
	struct sr_arpentry* mac_add = sr_arpcache_lookup(&(sr->cache), nexthop_rt->gw.s_addr);
	

	/*gets the interface of the next hop*/
	struct sr_if* interface = sr_get_interface(sr, nexthop_rt->interface);
	
	/*Equivalent Mac address was found*/	
	if(mac_add != NULL)
	{
		/*Creates the ethernet frame and sends it*/
		uint8_t* eth_pkt = NULL;
		unsigned int eth_pkt_len = sr_generate_eth_packet(&(eth_pkt), data, data_len, interface->addr, mac_add->mac,  ntohs(ether_type));
	
		/*sends the packet and frees the packet and the mac entry*/
		int err = sr_send_packet(sr, eth_pkt, eth_pkt_len, interface->name);
		if(!err)
		{
				free(eth_pkt);
		}
		free(mac_add);
		
		

	}
	/*creates and queues an ARP request*/
	else
	{
		/*Creates the ethernet frame with empty destination mac Address*/
		uint8_t* eth_pkt = NULL;
		unsigned int eth_pkt_len = sr_generate_eth_packet(&(eth_pkt), data, data_len, interface->addr, NULL,  ntohs(ether_type));
		
		sr_arpcache_queuereq(&(sr->cache), nexthop_rt->gw.s_addr, eth_pkt , eth_pkt_len, interface->name);
		free(eth_pkt);
	}
	
	
	

	return 0;
}


/**
  * sr, uint8_t*, len -> int
  * Updates the the ip packet fields and then Forwards an ip packet to the apropriate address 
  *
  * struct sr_instace8 sr: the sr instance of the router
  * uint8_t* ip_pkt: pointer to the ip packet
  * size_t len: length of the ip packet
  * struct sr_if* interface: the interface that received the ip packet
  *
  */
int sr_forward_packet(struct sr_instance* sr, uint8_t* ip_pkt, size_t len)
{
	sr_ip_hdr_t* ip_hdr = (sr_ip_hdr_t*) ip_pkt;
	
	/* updates the ttl and check sum */ 
	ip_hdr->ip_ttl--;
	/* time to live exceeded */	
	if(ip_hdr->ip_ttl == 0)
	{
		struct sr_rt* nexthop_rt = sr_longest_prefix_match( sr, ip_hdr->ip_src);
		/*Generates and sends the icmp message*/
		uint8_t* icmp_pkt = NULL;
		unsigned int icmp_len = sr_generate_icmp(&(icmp_pkt), icmp_type_time_exceeded, 0, 0, 0, ip_pkt, len);
		/*gets the interface*/
		struct sr_if* interface =  sr_get_interface(sr, nexthop_rt->interface);
		icmp_len =  sr_generate_ip(&(icmp_pkt), icmp_pkt,  icmp_len, interface->ip , ip_hdr->ip_src); 
		/*icmp_pkt now points to the whole ip packet and ready to be shipped*/ 
		sr_send_eth_packet(sr, icmp_pkt, icmp_len, nexthop_rt,ethertype_ip);
		
		free(ip_pkt);
		return icmp_type_time_exceeded;
	}

	ip_hdr->ip_sum = 0;
	ip_hdr->ip_sum = cksum(ip_hdr, sizeof(struct sr_ip_hdr));
	
	/*attempts to forward the packet*/
	struct sr_rt* nexthop_rt =  sr_longest_prefix_match(sr, ip_hdr->ip_dst);

	
	/*Destination net unreachable*/
	if(nexthop_rt == NULL)
	{
		/*gets the nexthop for the sender*/
		nexthop_rt =  sr_longest_prefix_match(sr, ip_hdr->ip_src);
		if(nexthop_rt == NULL)
		{
			/*couldn't reply to the sender either (means there is something wrong with the rt)*/
			return 2;
		}		
		

		/*generates and sends the icmp message*/
		uint8_t* icmp_pkt = NULL;
		unsigned int icmp_len = sr_generate_icmp(&(icmp_pkt), icmp_type_dest_unreachable, 0, 0, 0, ip_pkt, len);
		
		/*gets the interface name*/
		struct sr_if* interface = sr_get_interface(sr, nexthop_rt->interface);
		icmp_len =  sr_generate_ip(&(icmp_pkt), icmp_pkt,  icmp_len, interface->ip , ip_hdr->ip_src);
		struct sr_rt* nexthop_rt = sr_longest_prefix_match(sr,ip_hdr->ip_src);
		/*icmp_pkt now points to the whole ip packet and ready to be shipped*/ 
		sr_send_eth_packet(sr, icmp_pkt, icmp_len, nexthop_rt ,ethertype_ip);
		
		free(ip_pkt);
		return icmp_type_dest_unreachable;
	}
	/*sends the packet to correct destination*/
	else
	{
		sr_send_eth_packet(sr, ip_pkt, len,  nexthop_rt,  ethertype_ip);
	}
	return 0;	 
}


/* =======================================================================================================
* Broadcasting functions
*/


/** sr_broadcast_eth_packet
  * struct sr_instance*,  struct sr_arpreq* -> void
  * given an ARP request, encloses in a ethernet frame and broadcasts it to all the interfaces
  *
  * struct sr_instance* sr: the sr instance of the router
  *  struct sr_arpreq* arpreq: ARP request to be broadcasted
  *
  */
void sr_broadcast_arp_request(struct sr_instance* sr, struct sr_arpreq* arpreq)
{
	/*gets the next hop via LPM*/
	struct sr_rt* nexthop_rt = sr_longest_prefix_match(sr, arpreq->ip);
	struct sr_if* interface = sr_get_interface(sr, nexthop_rt->interface);


	uint32_t dst_ip = nexthop_rt->gw.s_addr;
		
	/*Generates the ARP request*/
	uint8_t* arp_req = NULL;
	int len = sr_generate_arp_request(&arp_req, interface, dst_ip);

	/*sets the broadcast dst mac*/
	unsigned char dst_mac [6];
	dst_mac[0] = 0xFF;dst_mac[1] = 0xFF;dst_mac[2] = 0xFF;dst_mac[3] = 0xFF;dst_mac[4] = 0xFF;dst_mac[5] = 0xFF;
	/*generates the ethernet packet*/		
	uint8_t* eth_pkt = NULL;
	unsigned int eth_pkt_len = sr_generate_eth_packet(&eth_pkt, arp_req, len, interface->addr, dst_mac,  ntohs(ethertype_arp));
	
	/*failed to create the ethernet packet*/
	if(eth_pkt_len <= 0)
	{
		if(eth_pkt != NULL)
		{
			free(eth_pkt);
		}
		return;
	}
	sr_send_packet(sr, eth_pkt, eth_pkt_len, interface->name);
	free(eth_pkt);
}

/** sr_send_all_arp_queue
  * struct sr_instance*, struct sr_arpreq* -> unsigned int
  *
  * Given an ARP Request, sends all the packets associated with this request
  * This function should be called whenever an ARP reply is received and the Mac Address was cached
  * returns the number of packets sent 
  *
  * struct sr_instance* sr: sr instance associated with the router
  * struct sr_arpreq* arpreq: ARP request that got its reply
  */
unsigned int sr_send_all_arp_queue(struct sr_instance* sr, struct sr_arpreq* arpreq)
{
	/*gets the ARP entry containing the address*/
	struct sr_arpentry* entry = sr_arpcache_lookup(&(sr->cache), arpreq->ip);
	
	unsigned int packetno = 0;
	/*iterates over the Ethernet Frames in the queue*/
	struct sr_packet* cursor = arpreq->packets;
	while(cursor != NULL)
	{
		sr_ethernet_hdr_t* eth_hdr = (sr_ethernet_hdr_t*) cursor->buf;
		/*Fills in the MAC address*/
		memcpy(eth_hdr->ether_dhost, entry->mac, ETHER_ADDR_LEN);
		int err = sr_send_packet(sr, cursor->buf, cursor->len, cursor->iface);
		if(!err)
		{
			packetno++;
		}
		cursor = cursor->next;
	}
	free(entry);
	return packetno;
}
/**
  * struct sr_instance*, struct sr_packet* -> unsigned int
  * Sends ICMP Destination Host unreachable for all the Packets in the given list
  * return the number of packets that has been sent 
  *
  * struct sr_instance* sr: sr instance of the router
  * struct sr_packet* lst_pkt: List of packets
  *
  */
unsigned int sr_broadcast_destination_host_unreach(struct sr_instance* sr, struct sr_packet* lst_pkt)
{
	struct sr_packet* cursor = lst_pkt;
	unsigned int messno = 0;
	/*iterates over the list*/
	while(cursor != NULL)
	{
		/*extracts necessary infromation from the packet*/
		struct sr_if* interface = sr_get_interface(sr, cursor->iface);
		struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*) (cursor->buf + sizeof(sr_ethernet_hdr_t));
		/*generates the icmp packet and encloses it in an ip*/
		uint8_t* icmp_pkt;
		unsigned int len = sr_generate_icmp(&(icmp_pkt), icmp_type_dest_unreachable, 1, 0, 0, (uint8_t*)ip_hdr, 
											cursor->len - sizeof(sr_ethernet_hdr_t));
		len = sr_generate_ip(&(icmp_pkt), icmp_pkt, len, interface->ip, ip_hdr->ip_src);
		
		/*LPM*/
		struct sr_rt* nexthop_rt = sr_longest_prefix_match(sr, ip_hdr->ip_src);	
		/*sends the icmp packet*/
		int err = sr_send_eth_packet(sr, icmp_pkt, len, nexthop_rt,  ethertype_ip);
		if(!err)
		{
			messno++;
		}

		cursor = cursor->next;
	}
	return messno;
}




/* ==============================================================================================================================================
   Packet Generating Functions
*/


/**
  * uint8_t**, uint8_t, uint8_t, uint16_t, uint16_t -> uint8_t
  * generates an icmp packet given the type and the code of the icmp message
  * the value of the icmp packet pointer is placed in pkt_ptr*
  * returns a the length of the packet or 0 on error
  * 
  * uint8_t** pkt_ptr: pointer to the the pointer of the icmp packet
  * uint8_t type: type of the icmp message (host byte-order)
  * uint8_t code: code of the icmp message (host byte-order)
  * uint16_t identifier: the identifier of an echo reply (Network byte-order)
  * uint16_t sequence: the sequence number of an echo request (Network byte-order)
  * The identifier and the sequence number maybe set to zero in case of icmp message other than echo reply
  *  uint8_t* original_packet: Pointer to the original packet assoicated with this ICMP message
  */
unsigned int sr_generate_icmp(uint8_t** pkt_ptr, uint8_t type, uint8_t code,
							  uint16_t identifier, uint16_t seqno,
							  uint8_t* original_pkt, size_t original_len)	
{
	/* puts the type and and code in the Network byte order */
	uint16_t type_code = code;
	type_code = type_code << 8;
	type_code = type_code | type;
	type_code = ntohs(type_code);
	int n_type = (type_code & 0xFF00) >> 8; 
	int n_code = (type_code & 0x00FF);

	int len = 0;

	/* allocates memory */
	sr_icmp_t3_hdr_t* icmp = malloc(sizeof(sr_icmp_t3_hdr_t) +  original_len);
	if(icmp == NULL)
	{
		return 0;
	}
	/* sets the fields to the correct values */
	icmp->icmp_type = n_type;	
	icmp->icmp_code = n_code;
	icmp->icmp_sum = 0;
	icmp->unused = identifier;
	icmp->next_mtu = seqno;

	/*copies the original packet to the icmp*/	
	if(n_type == 0 && n_code == 0)
	{
 		memcpy(icmp->data, original_pkt, original_len);
		len = original_len;
	}
	else
	{	
		memcpy(icmp->data, original_pkt,  ICMP_DATA_SIZE);	
		len =  ICMP_DATA_SIZE;
	}
	/*calculates the checksum*/
	icmp->icmp_sum = cksum(icmp, sizeof(sr_icmp_t3_hdr_t) +  len);


	*(pkt_ptr) = (uint8_t*) icmp;
	return sizeof(sr_icmp_t3_hdr_t) +  len;
	
}

/**
  * uint8_t**, uint8_t*, size_t uint32_t, uint32_t -> int
  *
  * Creates an IP packet with a source and destination ip adresses, then places a given data with a given length as
  * payload for the ip packet. It should free the given payload after copying it to the ip packet 
  * 
  * returns the total size of the ip packet including the payload; len(ip_header) + len(payload)
  * or 0 on error
  *
  * uint8_t** pkt_ptr: pointer to a pointer that will be pointing to the ip packet 
  * uint8_t* payload: payload of the ip packet 
  * size_t  payload_len: length of the payload in bytes
  * uint32_t ip_src: source ip adress of the sender (Network Byte-Order)
  * uint32_t ip_dst: destination ip adress of the receiver (Network-Order)
  *	
  */
unsigned int sr_generate_ip(uint8_t** pkt_ptr, uint8_t* payload, size_t payload_len, uint32_t ip_src, uint32_t ip_dst)
{
	/*Allocates memory for the ip packet*/
	int len = sizeof(sr_ip_hdr_t) + payload_len;
	sr_ip_hdr_t* ip_packet = malloc(len);
	if(ip_packet == NULL)
	{
		if(payload != NULL) free(payload);
		return 0;
	}

	/*sets the header fields with the correct byte-order*/
	ip_packet->ip_v = 4;
	ip_packet->ip_hl = (sizeof(sr_ip_hdr_t) / 4) & 0xF;
	ip_packet->ip_tos = 0;
	ip_packet->ip_len = htons(len);
	ip_packet->ip_id =htons(ip_identification++);
	ip_packet->ip_off = htons(IP_DF);
	ip_packet->ip_ttl = IP_TTL_DEF;
	ip_packet->ip_p = htons(1) >> 8;
	ip_packet->ip_sum = 0; /*postpone this to later*/
	ip_packet->ip_src = ip_src;
	ip_packet->ip_dst = ip_dst;

	/*copy the payload IF not NULL*/
    if(payload != NULL)
	{
		uint8_t* pkt_payload = ((uint8_t*) ip_packet) + sizeof(sr_ip_hdr_t);
		memcpy(pkt_payload, payload, payload_len);
		free(payload);
	}
	
	ip_packet->ip_sum = cksum(ip_packet, sizeof(sr_ip_hdr_t) + payload_len);
	/*sets the pointer and returns the length*/
	*(pkt_ptr) = (uint8_t*) ip_packet;
	return len;
}	


/** sr_generate_eth_packet
  * 
  * uint8_t**, uint8_t*, size_t, char[], char[], uint16_t -> size_t
  *
  * Generates an ehternet packet with given payload and sets the given pointer to the generated packet 
  * returns the length of the packet or 0 on error
  *
  * uint8_t** eth_pkt: pointer to the pointer that will point to the ethernet packet
  * uint8_t* data: the payload of the ethernet packet (should be freed)
  * size_t data_len: length of the payload
  * char [] src_mac: source mac address
  * char [] dst_mac: destination mac address 
  * uint16_t eth_type: type of the payload 
  *
  * Note: All header info must be in network byte-order
  */
unsigned int sr_generate_eth_packet(uint8_t** eth_pkt, uint8_t* data, size_t data_len,
							 unsigned char src_mac [], unsigned char dst_mac[],  uint16_t eth_type)
{
		/*Ethernet Frames must have a payload*/
		if(data == NULL)
		{
			return 0;
		}
		/*Allocates memory for the Ethernet packet*/
		sr_ethernet_hdr_t* eth_hdr = calloc(sizeof(sr_ethernet_hdr_t) + data_len, 1);
		if(eth_pkt == NULL)
		{
			return 0;
		}
		/*sets the header info*/
		memcpy(eth_hdr->ether_shost, src_mac, ETHER_ADDR_LEN);
		if(dst_mac != NULL)
		{
			memcpy(eth_hdr->ether_dhost, dst_mac, ETHER_ADDR_LEN);
		}

		eth_hdr->ether_type = eth_type;
	
		/*sets the ethernet payload*/
		uint8_t* eth_payload = ((uint8_t*) (eth_hdr)) + sizeof(struct sr_ethernet_hdr);
		memcpy(eth_payload, data, data_len);

		free(data);
		*(eth_pkt) = (uint8_t*) eth_hdr;
		return (sizeof(sr_ethernet_hdr_t) + data_len); 
}



/**
  * uint8_t**, struct sr_if*, uint32_t -> int
  * Generates a broadcast ARP request packet
  * returns the length of the ARP packet
  *
  * uint8_t** arp_ptr: pointer to the pointer that will point the generated ARP packet 
  *	struct sr_if* interface: interface that has the sender information 
  * uint32_t dst_ip: ip of the targer (Network Byte-Order)
  *
  */
unsigned int sr_generate_arp_request(uint8_t** arp_ptr, struct sr_if* interface, uint32_t dst_ip)
{
	/*Allocates memory for the arp packet*/
	sr_arp_hdr_t* arp = malloc(sizeof(sr_arp_hdr_t));
	if(arp == NULL)
	{
		return 0;
	}
	/*sets the fields*/
	arp->ar_hrd = ntohs(1);
	arp->ar_pro = ntohs(ethertype_ip);
	arp->ar_hln = (ntohs(ETHER_ADDR_LEN)) >> 8;
	arp->ar_pln = ntohs(4) >> 8;
	arp->ar_op = ntohs(arp_op_request);
	memcpy(arp->ar_sha, interface->addr, ETHER_ADDR_LEN);	
	arp->ar_sip = interface->ip;
	arp->ar_tip = dst_ip;

	/*sets broadcast destination MAC addr*/
	char dst_mac[6];
	dst_mac[0] = 0xFF;dst_mac[1] = 0xFF;dst_mac[2] = 0xFF;dst_mac[3] = 0xFF;dst_mac[4] = 0xFF;dst_mac[5] = 0xFF;
	memcpy(arp->ar_tha, dst_mac, ETHER_ADDR_LEN);



	/*sets the pointer and returns the length*/
	*(arp_ptr) = (uint8_t*) arp;
	return sizeof(sr_arp_hdr_t);
}

/**
  * uint8_t**, struct sr_if*, uint_32t, unsigned char[] -> int
  * Generates an arp reply 
  * returns the length of the ARP packet or 0 on failure
  *
  * uint8_t** arp_ptr: pointer to the pointer that will point the generated ARP packet 
  *	struct sr_if* interface: interface that has the sender information 
  * uint32_t dst_ip: ip of the targer (Network Byte-Order)
  * unsigned char[] dst_mac: Mac Adresss of the destination
  *
  */
unsigned int sr_generate_arp_reply(uint8_t** arp_ptr, struct sr_if* interface, uint32_t dst_ip, unsigned char dst_mac[])
{
	/*Allocates memory for the arp packet*/
	sr_arp_hdr_t* arp = malloc(sizeof(sr_arp_hdr_t));
	if(arp == NULL)
	{
		return 0;
	}
	/*sets the fields*/
	arp->ar_hrd = ntohs(1);
	arp->ar_pro = ntohs(ethertype_ip);
	arp->ar_hln = (ntohs(ETHER_ADDR_LEN)) >> 8;
	arp->ar_pln = ntohs(4) >> 8;
	arp->ar_op = ntohs(arp_op_reply);
	memcpy(arp->ar_sha, interface->addr, ETHER_ADDR_LEN);	
	arp->ar_sip = interface->ip;
	arp->ar_tip = dst_ip;
	memcpy(arp->ar_tha, dst_mac, ETHER_ADDR_LEN);
	
	/*sets the pointer and returns the length*/
	*(arp_ptr) = (uint8_t*) arp;
	return sizeof(sr_arp_hdr_t);
}

/*=================================================================================================================================================
  Utils and helpers
*/


/**
  * uint16_t* -> sr_icmp_hdr_t
  * Extracts the icmp header info, stores it in a struct sr_icmp_header, and returns this struct
  * 
  */
sr_icmp_hdr_t sr_resolve_icmp_hdr(uint16_t* icmp)
{
	sr_icmp_hdr_t icmp_hdr;
	/*the ckecksum is returned in network byte-order*/ 
	icmp_hdr.icmp_sum = icmp[1];

	/*the type and code are converted to host byte-order*/
	icmp_hdr.icmp_type = icmp[0] & 0x00FF;
	icmp_hdr.icmp_code = (icmp[0] & 0xFF00) >> 8;
	return icmp_hdr; 
}


/** Longest prefix match
  *
  * struct sr_instance*, uint32_t -> struct sr_rt* 
  *
  * preforms a longest prefix match on a given ip address
  * returns the next hop
  *
  * struct sr_instance*: sr instance of the router
  * uint32_t ip: a 32 bits ip adress
  */

struct sr_rt* sr_longest_prefix_match(struct sr_instance* sr, uint32_t ip)
{
	struct sr_rt* cursor = sr->routing_table;
	uint32_t longest_mask = 0;
	struct sr_rt* rt_entry = NULL;


	/*loops through the routing table*/
	while(cursor != NULL)
	{
		/*ANDs the prefix with the netmask*/
		uint32_t prefix = (cursor->dest.s_addr) & (cursor->mask.s_addr);
		uint32_t ip_cmp = ip & (cursor->mask.s_addr);

		/*compares the prefixes*/
		if(prefix == ip_cmp)
		{
			/*checks if this prefix is longer than the previous*/
			uint32_t h_mask = ntohl(cursor->mask.s_addr);
			if(longest_mask <= h_mask)
			{
				longest_mask = h_mask;
				rt_entry = cursor;
			}
		}
		cursor = cursor->next;
	}

	return rt_entry;

}
